/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

/**
 *
 * @author yordyg
 */
public class Perfil {
    private int claveModulo;
    private String modulo;
    private boolean acceso;

    public int getClaveModulo() {
        return claveModulo;
    }

    public void setClaveModulo(int claveModulo) {
        this.claveModulo = claveModulo;
        
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public boolean isAcceso() {
        return acceso;
    }

    public void setAcceso(boolean acceso) {
        this.acceso = acceso;
    }
}
