/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.util.HashMap;

/**
 *
 * @author yordyg
 */
public class Usuario {
    private int idUsuario;
    private String nombre;
    private String nombreCompleto;
    private HashMap modulos;
    private boolean accedio;

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public boolean isAccedio() {
        return accedio;
    }

    public void setAccedio(boolean accedio) {
        this.accedio = accedio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public HashMap getModulos() {
        return modulos;
    }

    public void setModulos(HashMap modulos) {
        this.modulos = modulos;
    }
}
