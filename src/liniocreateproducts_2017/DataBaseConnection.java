/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package liniocreateproducts_2017;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author yordyg
 */
public class DataBaseConnection {
    private String              userName,passwordName,dataBaseName;
    private CallableStatement   callSP=null;
    private ResultSet           RS=null;
    private Connection          connection=null;
    //Class Constructor 
    public DataBaseConnection(String User,String Password,String DB){
        this.userName=User;
        this.passwordName=Password;
        this.dataBaseName=DB;
    }
    
    public Connection getConnectionDB(){
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            connection=DriverManager.getConnection("jdbc:sqlserver://192.168.1.26:1433;databaseName="+this.dataBaseName+";user="+this.userName+";password="+this.passwordName+";");
            Thread.sleep(3000);
            System.out.println("Database Connected");
        }catch(Exception e){
            System.out.println("Error to connecting to database\nError type: "+e);
        }
        return connection;
        
    }
    
    public ResultSet getSetProducts(Connection connection) throws SQLException{
        try{
            callSP=connection.prepareCall("{call GetNewProductsLINIO}");
            callSP.execute();
            RS=callSP.getResultSet();
        }catch(Exception e){
            System.out.println("Error to call the Stored Procedures\nError type: "+e);
        }
        
        
        //......................Test Block......................................
        //......................................................................    
//        int count=0;
//        while(RS.next()){
//            count++;
//            if(RS.getString(50).equalsIgnoreCase("CALZADO")){
//                System.out.println("RESULTADO YY:\n");
//                System.out.println(RS.getString(50)+" Si es calzado");
//            }else{
//                System.out.println("RESULTADO YY:\n");
//                System.out.println(RS.getString(50)+" Es distinto a calzado");
//            }
//        }
//        System.out.println("TOTAL: "+count);
        //......................................................................    
        //......................................................................    
            
        return RS;
    }
    
    public void sendToErrorsTable(Connection connection,String SKU,String Error){
        
            String queryInsertErrors=               "insert into SAP_AUX.DBO.ErrorsToCreateProductsLINIO([ItemCode],[Error]) " +
                                                    "values (?,?)";
            try {
                PreparedStatement statementQuery=(connection).prepareStatement(queryInsertErrors);
                statementQuery.setString(1, SKU);
                statementQuery.setString(2, Error);
                int respuesta=statementQuery.executeUpdate();
                
                if(respuesta==1){
                    System.out.println("Se inserto el error");
                }else{
                    System.out.println("Error al insertar error");
                }
            } catch (SQLException ex) {
                Logger.getLogger(Bolsa.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    
    //Getters and Setter
    public void setUserName(String user){
        this.userName=user;
    }
    public String getUserName(){
        return this.userName;
    }
    public void setPasswordName(String Password){
        this.passwordName=Password;
    }
    public String getPasswordName(){
        return this.passwordName;
    }
    public void setDataBaseName(String DB){
        this.dataBaseName=DB;
    }
    public String getDataBaseName(){
        return this.dataBaseName;
    }
    public Connection getConnection() {
        return connection;
    }
    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
