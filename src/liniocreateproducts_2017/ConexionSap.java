    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package liniocreateproducts_2017;
import java.io.IOException;
import java.util.Properties;
import com.sap.smb.sbo.api.*;
import Data.Perfil;
import Data.Usuario;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
/**
 *
 * @author luzg
 */
public class ConexionSap {
       
    private static String servidor;
    private static String servidor2;
    private static String baseDatos;
    private static String baseDatos2;
    private static String usrSap;
    private static String usrSql;
    private static String claSap;
    private static String claSql;
    private static int tipoBase;
    private static int puerto;
    private static int consignacion;
    private static ICompany company;
    private static String usuario;
    
    
    public ConexionSap() {
        cargaInformacion();
    }
    
    private void cargaInformacion(){
        try {
            Properties propiedades = new Properties();
            propiedades.load(getClass().getResourceAsStream("conexion.properties"));
            if (!propiedades.isEmpty()) {                
                servidor = propiedades.getProperty("servidor");
                servidor2 = propiedades.getProperty("servidor2");
                baseDatos = propiedades.getProperty("baseDatos");
                baseDatos2 = propiedades.getProperty("baseDatos2");
                usrSap = propiedades.getProperty("usrSAP");
                usrSql = propiedades.getProperty("usrSQL");
                claSap = propiedades.getProperty("claveSAP");
                claSql = propiedades.getProperty("claveSQL");
                tipoBase = Integer.parseInt(propiedades.getProperty("tipoServer"));
                puerto = Integer.parseInt(propiedades.getProperty("puertoSAP"));
                consignacion = Integer.parseInt(propiedades.getProperty("consignacion"));
            } else {
                System.out.println("No se a creado el archivo de configuracion");
            }
        } catch(IOException | NumberFormatException nfe) {
            if(nfe instanceof NumberFormatException){
                System.out.println("El valor del tipo de servidor es incorrecto");
            } else{
                System.out.println("Ocurrio el siguiente error: " + nfe.getMessage());
            }
        }   
    }

    public static String getServidor2() {
        return servidor2;
    }

    public static String getUsuario() {
        return usuario;
    }

    public static void setUsuario(String usuario) {
        ConexionSap.usuario = usuario;
    }

    /**
     * @return the servidor
     */
    static String getServidor() {
        return servidor;
    }

    /**
     * @return the baseDatos
     */
    static String getBaseDatos() {
        return baseDatos;
    }
    
    public static void setBaseDatos(String baseDatos) {
        ConexionSap.baseDatos = baseDatos;
    }

    /**
     * @return the baseDatos2
     */
    static String getBaseDatos2() {
        return baseDatos2;
    }

    /**
     * @return the usrSap
     */
    static String getUsrSap() {
        return usrSap;
    }

    /**
     * @return the usrSql
     */
    static String getUsrSql() {
        return usrSql;
    }

    /**
     * @return the claSap
     */
    static String getClaSap() {
        return claSap;
    }

    /**
     * @return the claSql
     */
    static String getClaSql() {
        return claSql;
    }

    /**
     * @return the tipoBase
     */
    static int getTipoBase() {
        return tipoBase;
    }
    
    /**
     * @return the puerto
     */
    static int getPuerto() {
        return puerto;
    }
    
    /**
     * @return the consignacion
     */
    static int getConsignacion() {
        return consignacion;
    }

    /**
     * @return the company
     */
    static ICompany getCompany() {
        return company;
    }
    
    static boolean conectaSAP() throws Exception{
        boolean estatus = false;
        try {
            company = SBOCOMUtil.newCompany();
            company.setServer(getServidor());
            company.setCompanyDB(getBaseDatos());
            company.setDbUserName(getUsrSql());
            company.setDbPassword(getClaSql());
            company.setUserName(getUsrSap());
            company.setPassword(getClaSap());
            company.setLanguage(SBOCOMConstants.BoSuppLangs_ln_Spanish_La);
            company.setLicenseServer("192.168.1.35:30000");
            company.setDbServerType(SBOCOMConstants.BoDataServerTypes_dst_MSSQL2012);
            //company.setUseTrusted(false);

            int result = company.connect();
            if (result != 0) {
                throw new Exception("No es posible establecer conexion SAP numero" + result);
            } else  {
                Logger.getLogger(ConexionSap.class.getName()).log(Level.INFO, "Se realizo la conexion correctamente a: " + company.getCompanyName(), "");
                estatus = true;
            }
        } catch(Exception ex) {
            throw ex;
        }
        return estatus;
    }
    
    static void cerrarConexionSAP() throws Exception {
        try{
            if (company!=null && company.isConnected()) {
                company.disconnect();
            } else {
                company = null;
            }
        } catch(Exception ex) {
            throw ex;
        }
    }
    
    static boolean statusConexion(){
        boolean valor;
        try{
            valor = company.isConnected();
        } catch(Exception ex) {
            valor = false;
        }
        return valor;    
    }
    
    static IRecordset busqueda(String consulta) throws Exception {
        IRecordset almacen;
        
        if(company.isConnected()) {
            try {
                almacen = SBOCOMUtil.newRecordset(company);
                almacen.doQuery(consulta);
            } catch(Exception ex){
                throw ex;
            }
        } else {
            Logger.getLogger(ConexionSap.class.getName()).log(Level.SEVERE, "No hay conexion a base de datos", "");
            almacen = null;
        }
        return almacen;
    }
    
    static boolean comando(String consulta) throws Exception {
        IRecordset almacen;
        boolean valor;
        if(company.isConnected()) {
            try {
                almacen = SBOCOMUtil.newRecordset(company);
                almacen.doQuery(consulta);
                valor = true;
            } catch(Exception ex){
                valor = false;
                throw ex;
            }
        } else {
            Logger.getLogger(ConexionSap.class.getName()).log(Level.SEVERE, "No hay conexion a base de datos", "");
            almacen = null;
            valor = false;
        }
        return valor;
    }
    
    static String retornaCadena(String consulta) throws Exception{
        String valor = "";
        IRecordset registro;
        try{
            registro = busqueda(consulta);
            if(registro.getRecordCount()>0) {
                valor = String.valueOf(registro.getFields().item(0).getValue());
            }
        } catch(Exception ex){
             throw ex;
        }
        return valor;
    }
    
    static IUsers regresaUsuario(String nombre, String pass) throws Exception{
        IUsers usuarioSap;
        String claveUsuario;
        String sql;
        usuarioSap = SBOCOMUtil.newUsers(company);
        sql = "SELECT USERID FROM OUSR WHERE (USER_CODE = '" + nombre + "')";
        try{
            claveUsuario = retornaCadena(sql);
            if(claveUsuario.equals("")) {
                usuarioSap=null;
            } else {
                usuarioSap.getByKey(Integer.parseInt(claveUsuario));
            }
        } catch(Exception ex){
            throw ex;
        }
        return usuarioSap;
    }
    
    static IRecordset listaSociedades() throws Exception{
        IRecordset sociedades = null;
        try{
            if(company.isConnected()){
                sociedades = company.getCompanyList();
            }
        } catch(Exception ex){
            throw ex;
        }
        return sociedades;
    }
    
    static void iniciaTransaccion() throws Exception{
        try{
            if(company.isConnected()){
                company.startTransaction();
            }
        } catch(Exception ex){
           throw ex;
        }
    }
    
    static void terminaTransaccion(int valor) throws Exception{
        try{
            if(company.isConnected()) {
                company.endTransaction(valor);
            }
        } catch(Exception ex){
           throw ex;
        }
    }
    
    static Usuario validaUsuario(String usuario, String contra){
        Usuario usu = new Usuario();
        IRecordset registro;
        Perfil modulo;
        HashMap permisos;
        String consulta;
        String sContra;
        String activo;
        int x = 0;
        try {
            if(getServidor().equals(getServidor2())){
                consulta = "SELECT T0.ID_Usuario, T0.NombreCompleto, T1.ID_Perfil, T1.Modulo, T1.acceso, T0.Contra, T0.activo " +
                "  FROM [" + getBaseDatos2() + "].[dbo].[SYN_SAP_USUARIO] AS T0 " +
                "  LEFT JOIN [" + getBaseDatos2() + "].[dbo].[SYN_SAP_PERFIL] AS T1 ON T1.ID_Usuario = T0.ID_Usuario " +
                "  WHERE [Nombre] = '" + usuario + "'";
            } else {
                consulta = "SELECT T0.ID_Usuario, T0.NombreCompleto, T1.ID_Perfil, T1.Modulo, T1.acceso, T0.Contra, T0.activo " +
                "  FROM [" + getServidor2()+ "].[" + getBaseDatos2() + "].[dbo].[SYN_SAP_USUARIO] AS T0 " +
                "  LEFT JOIN [" + getServidor2()+ "].[" + getBaseDatos2() + "].[dbo].[SYN_SAP_PERFIL] AS T1 ON T1.ID_Usuario = T0.ID_Usuario " +
                "  WHERE [Nombre] = '" + usuario + "'";
            }
            registro = busqueda(consulta);
            if(registro.getRecordCount()>0){
                registro.moveFirst();
                usu.setNombre(registro.getFields().item(1).getValue().toString());
                usu.setNombre(usuario);
                usu.setIdUsuario(Integer.valueOf(registro.getFields().item(0).getValue().toString()));
                sContra = registro.getFields().item(5).getValue().toString();
                sContra = deEncripta(sContra);
                activo = (String) registro.getFields().item(6).getValue();
                permisos = new HashMap();
                if(sContra.equals(contra) && activo.equals("1")) {
                    usu.setAccedio(true);
                    while(registro.getRecordCount()>x) {
                        modulo = new Perfil();
                        modulo.setAcceso(Boolean.parseBoolean(registro.getFields().item(4).getValue().toString()));
                        modulo.setClaveModulo(Integer.valueOf(registro.getFields().item(2).getValue().toString()));
                        modulo.setModulo(registro.getFields().item(3).getValue().toString());
                        permisos.put(modulo.getClaveModulo(), modulo);
                        x++;
                    }
                } else {
                    usu.setAccedio(false);    
                }
                usu.setModulos(permisos);
            }
        } catch(Exception ex) {
            
        }
        return usu;
    }
    
    public static String encripta(String cadena) {
        StandardPBEStringEncryptor s = new StandardPBEStringEncryptor();
        s.setPassword("CLOE2key");
        return s.encrypt(cadena);
    }
    
    private static String deEncripta(String cadena) {
        StandardPBEStringEncryptor s = new StandardPBEStringEncryptor();
        s.setPassword("CLOE2key");
        String devuelve = "";
        try {
            devuelve = s.decrypt(cadena);
        } catch (Exception e) {
        }
        return devuelve;
    }
    
    public static String cambiaPassword(String usuario, String antiguo, String nuevo) {
        String sql;
        String servidor = "";
        IRecordset datos;
        String clave;
        String error;
        if(!getServidor().equals(getServidor2())) {
            servidor = "[" + getServidor2() + "].";
        }
        sql = "SELECT ID_Usuario, Contra FROM " + servidor + "[" + getBaseDatos2() + "].[dbo].[SYN_SAP_USUARIO] " +
                  " WHERE Nombre = '" + usuario + "' AND activo = 1";
        try{
            datos = busqueda(sql);
            if(datos.getRecordCount()>0) {
                clave = datos.getFields().item(1).getValue().toString();
                clave = deEncripta(clave);
                if(clave.equals(antiguo)){
                    sql = "UPDATE " + servidor + "[" + getBaseDatos2() + "].[dbo].[SYN_SAP_USUARIO] " +
                            " SET [Contra] = '" + encripta(nuevo) + "' " +
                            " WHERE ID_Usuario = '" + datos.getFields().item(0).getValue().toString() + "'";
                    if(comando(sql)) {
                        error = "";
                    } else {
                        error = "No es posible realizar la actualizacion de contraseña";
                    }
                } else {
                    error = "Clave antigua incorrecta";
                }
            } else {
                error = "El usuario no existe o se encuentra inactivo";
            }
        } catch(Exception ex){
            error = "Error debido a: " + ex.getMessage();
        }
        return error;
    }
      
}
