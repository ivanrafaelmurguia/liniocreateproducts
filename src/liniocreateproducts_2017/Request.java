/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package liniocreateproducts_2017;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sap.smb.sbo.api.SBOCOMException;
import com.sellercenter.bss.BaseBss;
import com.sellercenter.entity.ErrorAPI;
import com.sellercenter.entity.Head;
import java.io.StringWriter;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 *
 * @author yordyg
 */
public class Request {
    private     ResultSet   resSet=null;
    private     int         count=0,vAux;
    private     float       vAuxF;
    private     Product     product;
    private     ArrayList<String>   noCumplieron = new ArrayList<String>();
    ArrayList<String> cumplen=new ArrayList<String>();
    static      Map<String, String> params = new HashMap<String, String>();
     String XML = "";
     String responseType = "";
     static BaseBss objBss = new BaseBss();
    
    public Product CreateFileOfProducts(ResultSet resSet,DataBaseConnection connection) throws SQLException, SBOCOMException, ParserConfigurationException, TransformerConfigurationException, TransformerException{
        
        SapProperties prop=new SapProperties();
        prop.setResultSP(resSet);
        //DataBaseConnection connection=new DataBaseConnection("sa","B1Admin","SAP_AUX");
        //connection.setConnection(connection.getConnectionDB());
//        System.out.println("--------------------------");
//        System.out.println("Connecting to database.....");
//        DataBaseConnection connection=new DataBaseConnection("sa","B1Admin","SAP_AUX");
//        connection.getConnectionDB();
//        System.out.println("Successful DB connection!");
//        System.out.println("--------------------------\n\n");
        params.put("Format", "JSON");
        params.put("Action", "ProductCreate");
        
        
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        Document doc = docBuilder.newDocument();
        Element rootElement= doc.createElement("Request");
        doc.appendChild(rootElement);
       

        //......Get our new products set.......
        //resSet=connection.getSetProducts(connection.getConnection());
        
        while(resSet.next()){
            count++; //Contamos el total de registros para pasarlo al objeto sapPropiedades
            System.out.println(resSet.getString(1));
                if(resSet.getString(50).equalsIgnoreCase("CALZADO")){
                    product=new Calzado();
                }else{
                    product = new Bolsa();
                }
        //............Give properties to object..................
            
            
        product.setNombre(resSet.getString(1));
        product.setMarca(resSet.getString(2));
        product.setModelo(resSet.getString(3));
        product.setDescripcion(resSet.getString(4));
        product.setColor(resSet.getString(6));
        product.setMedidasProducto(resSet.getString(8));
        product.setPesoProducto(resSet.getString(9));
            
        if(!(resSet.getString(10)==null)){
            vAuxF=Float.parseFloat(resSet.getString(10));
            vAux=(int)vAuxF;
            product.setCategoriaPrimaria(vAux+"");
        } 
            
        if(!(resSet.getString(11)==null)){    
        vAuxF=Float.parseFloat(resSet.getString(11));
        vAux=(int)vAuxF;
        product.setCategoriaAdicional1(vAux+"");
        }
        
        product.setPrecio(resSet.getString(14));
        product.setSkuVendedor(resSet.getString(18));
            System.out.println("---------");
            System.out.println(product.getSkuVendedor());
            System.out.println("---------");
        product.setSkuPadre(resSet.getString(19));
        product.setVariacion(resSet.getString(20));
        product.setCodigoBarras(resSet.getString(21));
            
        if(!(resSet.getString(22)==null)){    
        vAuxF=Float.parseFloat(resSet.getString(22));
        vAux=(int)vAuxF;
        product.setCantidad(vAux+"");
        }
        
        product.setGenero(resSet.getString(23));
        product.setColorPrincipalProducto(resSet.getString(25));
        product.setCondicionProducto(resSet.getString(30));
        product.setTiempoGarantia(resSet.getString(32));
        product.setAltoPqt(resSet.getString(34));
        product.setAnchoPqt(resSet.getString(35));
        product.setLargoPqt(resSet.getString(36));
        product.setPesoPqt(resSet.getString(37));
        product.setImpuestos(resSet.getString(39));
        product.setImgPrincipal(resSet.getString(42));
        product.setImg2(resSet.getString(43));
        product.setImg3(resSet.getString(44));
        product.setImg4(resSet.getString(45));
        product.setImg5(resSet.getString(46));
            //here we checking which structure we'll use to send at API, if CALZADO or BOLSA;
                //Instantiate object like "Calzado"
                //product = new Calzado();
                if(product.validatePhoto(product.getSkuVendedor())){
                    if(product.validateCompleteProducto(connection)){
                        //ADD PRODUCT TO FILE 
                        //---------------------------------------------------
                        //---------------------------------------------------
                        //---------------------------------------------------
                            product.createProduct(doc,rootElement);
                            cumplen.add(resSet.getString(18));
                            System.out.println("--------------");
                            System.out.println("Se a�adio un elemento   ");
                            System.out.println("--------------");
                        //---------------------------------------------------
                        //---------------------------------------------------
                        //---------------------------------------------------
                    }else{
                        System.out.println("The product "+product.getSkuVendedor()+" doesnt have the main characteristics, it was added to errors table (table name: ErrorsToCreateProductsLINIO, into SAP_AUX data base)");
                        noCumplieron.add(product.getSkuVendedor());
                    }
                }else{
                    //hasn't photo
                    //DataBaseConnection connection2=new DataBaseConnection("sa","B1Admin","SAP_AUX");
                    connection.sendToErrorsTable(connection.getConnection(), product.getSkuVendedor(), " Falta foto");
                    noCumplieron.add(product.getSkuVendedor());
                }
        }
        
        
        prop.setNoCumplieron(noCumplieron);
        prop.setTotalResultSet(count);
        
        //----------------------------------------------------------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------------------------------------------
        //ENVIAMOS EL ARCHIVO ARMADO
        doc.setXmlStandalone(true);
        DOMSource domSource = new DOMSource(doc);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.transform(domSource, result);
        XML=writer.toString();
        //--------------------------------------------------
        
        System.out.println(XML);
        System.out.println(XML);
                        
                        System.out.println("--------------------------------------------");
                        System.out.println("--------------------------------------------");
                        
                        System.out.println(XML);
                        
                        System.out.println("--------------------------------------------");
                        System.out.println("--------------------------------------------");
                        String json = objBss.getSellercenterApiResponseLinio(params, XML); // provide XML as an empty string when not needed
                        
                        if (json.equals("")) {
                              System.out.println("NO1 responde json");
                        } else {
                              Gson gson = new Gson();
                              Gson gsonPretty = new GsonBuilder().setPrettyPrinting().create();
                              JsonParser parser = new JsonParser();
                              JsonElement element = parser.parse(json);
                              if (element.isJsonObject()) {
                                    JsonObject mainJsonObject = element.getAsJsonObject();
                                    for (Map.Entry<String, JsonElement> entry : mainJsonObject.entrySet()) {
                                          String mainKey = entry.getKey();
                                          JsonElement mainValue = entry.getValue();
                                          System.out.println("");
                                          System.out.println("SI1 "+mainKey);
//                                                System.out.println("ARTICULO "+datos.getFields().item(0).getValue().toString());
                                          System.out.println("ARTICULO PRUEBA");
                                          if (mainKey.equalsIgnoreCase("SuccessResponse")) {
                                                  System.out.println("RESPUESTA BIEN");
//                                                      //------------Cambiamos bandera 19 a true para los articulos que se cargaron a la plataforma---------------------
                                                        //---------------------------------------------------------------------------------------------------------------
                                                        //---------------------------------------------------------------------------------------------------------------
                                                        //---------------------------------------------------------------------------------------------------------------
                                                        
                                                            prop.changeProperty19ToTrue(cumplen);
                                                  
                                                  
                                                        //---------------------------------------------------------------------------------------------------------------
                                                        //---------------------------------------------------------------------------------------------------------------
                                                        //---------------------------------------------------------------------------------------------------------------
                                                    
                                                
                                          } else if (mainKey.equalsIgnoreCase("ErrorResponse")) {
                                                
                                                System.out.println("RESPUESTA MAL");
                                                //ErrorResponse
                                                JsonObject outerObject = mainValue.getAsJsonObject();
                                                for (Map.Entry<String, JsonElement> outerEntry : outerObject.entrySet()) {
                                                        String outerKey = outerEntry.getKey();
                                                        JsonElement outerValue = outerEntry.getValue();

                                                        if (outerKey.equalsIgnoreCase("Head")) {
                                                              JsonObject innerObject = outerValue.getAsJsonObject();
                                                              Head headObject = gson.fromJson(innerObject, Head.class);
                                                              responseType = headObject.getResponseType();
                      //                        System.out.println(headObject.getResponseType());

                                                            // Println Informativo        
                                                            System.out.println("Head==> " + gsonPretty.toJson(headObject));

                                                            // Println Informativo  
                                                            for (Map.Entry<String, JsonElement> headEntry : innerObject.entrySet()) {
                                                                  String headKey = headEntry.getKey();
                                                                  JsonElement headValue = headEntry.getValue();
//                                                   System.out.printf("name=%s, value=%s\n", headKey, headValue.getAsString());
                                                            }

                                                            ErrorAPI orderItem = gson.fromJson(innerObject, ErrorAPI.class);
//                                                            sqlError = "INSERT INTO [SAP_AUX].[dbo].[MKP_LOG_ERROR]"
//                                                                    + "           ([ID_Orden]"
//                                                                    + "           ,[Tipo_Error]"
//                                                                    + "           ,[Codigo_Error]"
//                                                                    + "           ,[Mensaje])"
//                                                                    + "     VALUES"
//                                                                    + "           (" + orderItem.getErrorCode()
//                                                                    + "           ,'" + orderItem.getErrorType() + "'"
//                                                                    + "           ,'" + orderItem.getRequestAction() + "'"
//                                                                    + "           ,'" + orderItem.getErrorMessage() + "'";
//                                                            try {
//                                                                  inserta = ConexionSap.comando(sqlError);
//                                                            } catch (Exception ex) {
//                                                                  Logger.getLogger(CreateProduct.class.getName()).log(Level.SEVERE, null, ex);
//                                                            }
                                                      }
                                                }
                                          }
                                          
                                          
                                          
                                    }
                              }
                        }
                           System.out.println("Alta de productos en LINIO");
        
                //----------------------------------------------------------------------------------------------------------------------------
                //----------------------------------------------------------------------------------------------------------------------------
                //----------------------------------------------------------------------------------------------------------------------------
                //----------------------------------------------------------------------------------------------------------------------------

        return null;
    }

    public ArrayList<String> getNoCumplieron() {
        return noCumplieron;
    }
   
}
