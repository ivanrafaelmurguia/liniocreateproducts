/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package liniocreateproducts_2017;

import com.sap.smb.sbo.api.ICompany;
import com.sap.smb.sbo.api.Items;
import com.sap.smb.sbo.api.SBOCOMException;
import com.sap.smb.sbo.api.SBOCOMUtil;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author yordyg
 */
public class SapProperties {
    private ArrayList<String> noCumplieron=new ArrayList<String>();
    private ResultSet resultSP;
    private int totalResultSet;
    private boolean cambiaBandera=true;
    private static ICompany company;
    
    public void changeProperty19ToTrue(ArrayList<String> cumplen) throws SQLException, SBOCOMException{
        
        company=ConexionSap.getCompany();
        System.out.println("Activando Propiedad 19 de SAP ..........");
        for (int i = 0; i<cumplen.size(); i++) {
            try{
                Items Product;
                Product = (Items)SBOCOMUtil.newItems(company);
                Product.getByKey(cumplen.get(i));
                Product.setProperties(19, 1);
                Product.update();
            }catch(Exception e){
                System.out.println("Error al activar propiedad 19 del articulo: "+cumplen.get(i));
            }
        }
        
    }
    
    
    

    public ArrayList<String> getNoCumplieron() {
        return noCumplieron;
    }

    public void setNoCumplieron(ArrayList<String> noCumplieron) {
        this.noCumplieron = noCumplieron;
    }

    public ResultSet getResultSP() {
        return resultSP;
    }

    public void setResultSP(ResultSet ResultSP) {
        this.resultSP = ResultSP;
    }

    public int getTotalResultSet() {
        return totalResultSet;
    }

    public void setTotalResultSet(int totalResultSet) {
        this.totalResultSet = totalResultSet;
    }
    
}
