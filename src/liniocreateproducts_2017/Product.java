/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package liniocreateproducts_2017;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author yordyg
 */
abstract class Product {
    //Propierties of our object
    private String nombre,marca,modelo,descripcion,color,medidasProducto,pesoProducto,categoriaPrimaria,categoriaAdicional1,precio;
    private String skuVendedor,skuPadre,variacion,codigoBarras,cantidad,genero,colorPrincipalProducto,condicionProducto,tiempoGarantia,altoPqt,anchoPqt;
    private String largoPqt,pesoPqt,impuestos,imgPrincipal,img2,img3,img4,img5;
    private File   pathPhoto;
    private int count=0;
    private boolean flat=false;
    
    //Our Abstract Method
    abstract boolean validatePhoto(String sku);
    abstract boolean validateCompleteProducto(DataBaseConnection connection);
    abstract void createProduct(Document doc,Element rootElement);
    
    
    
    //method to find photo
//    public boolean validatePhoto(String SKU,String type){
//        if(type.equalsIgnoreCase("CALZADO")){
//            String subSKU=SKU.substring( 1, (SKU.length()-2) );
//            pathPhoto =  new File("\\\\192.168.1.34\\FTP_Fotografias\\"+subSKU+"40\\"+subSKU+"40_1.jpg");
//        }else{
//            pathPhoto =  new File("\\\\192.168.1.34\\FTP_Fotografias\\"+SKU+"\\"+SKU+"_1.jpg");
//        }
//        flat = pathPhoto.exists();
//        
//    return flat;
//    }

    
    
    
    
    
    
    
    
    
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = this.count+count;
    }

   
    
   
    //Create our methods Getters and setters 
     public File getPathPhoto() {
        return pathPhoto;
    }

    public void setPathPhoto(File pathPhoto) {
        this.pathPhoto = pathPhoto;
    }

    public boolean getFlat() {
        return flat;
    }

    public void setFlat(boolean flat) {
        this.flat = flat;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMedidasProducto() {
        return medidasProducto;
    }

    public void setMedidasProducto(String medidasProducto) {
        this.medidasProducto = medidasProducto;
    }

    public String getPesoProducto() {
        return pesoProducto;
    }

    public void setPesoProducto(String pesoProducto) {
        this.pesoProducto = pesoProducto;
    }

    public String getCategoriaPrimaria() {
        return categoriaPrimaria;
    }

    public void setCategoriaPrimaria(String categoriaPrimaria) {
        this.categoriaPrimaria = categoriaPrimaria;
    }

    public String getCategoriaAdicional1() {
        return categoriaAdicional1;
    }

    public void setCategoriaAdicional1(String categoriaAdicional1) {
        this.categoriaAdicional1 = categoriaAdicional1;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getSkuVendedor() {
        return skuVendedor;
    }

    public void setSkuVendedor(String skuVendedor) {
        this.skuVendedor = skuVendedor;
    }
    
    public String getSkuPadre() {
        return skuPadre;
    }

    public void setSkuPadre(String skuPadre) {
        this.skuPadre = skuPadre;
    }
    
    public String getVariacion() {
        return variacion;
    }

    public void setVariacion(String variacion) {
        this.variacion = variacion;
    }
    
    public String getCodigoBarras() {
        return codigoBarras;
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getColorPrincipalProducto() {
        return colorPrincipalProducto;
    }

    public void setColorPrincipalProducto(String colorPrincipalProducto) {
        this.colorPrincipalProducto = colorPrincipalProducto;
    }

    public String getCondicionProducto() {
        return condicionProducto;
    }

    public void setCondicionProducto(String condicionProducto) {
        this.condicionProducto = condicionProducto;
    }

    public String getTiempoGarantia() {
        return tiempoGarantia;
    }

    public void setTiempoGarantia(String tiempoGarantia) {
        this.tiempoGarantia = tiempoGarantia;
    }

    public String getAltoPqt() {
        return altoPqt;
    }

    public void setAltoPqt(String altoPqt) {
        this.altoPqt = altoPqt;
    }

    public String getAnchoPqt() {
        return anchoPqt;
    }

    public void setAnchoPqt(String anchoPqt) {
        this.anchoPqt = anchoPqt;
    }

    public String getLargoPqt() {
        return largoPqt;
    }

    public void setLargoPqt(String largoPqt) {
        this.largoPqt = largoPqt;
    }

    public String getPesoPqt() {
        return pesoPqt;
    }

    public void setPesoPqt(String pesoPqt) {
        this.pesoPqt = pesoPqt;
    }

    public String getImpuestos() {
        return impuestos;
    }

    public void setImpuestos(String impuestos) {
        this.impuestos = impuestos;
    }

    public String getImgPrincipal() {
        return imgPrincipal;
    }

    public void setImgPrincipal(String imgPrincipal) {
        this.imgPrincipal = imgPrincipal;
    }

    public String getImg2() {
        return img2;
    }

    public void setImg2(String img2) {
        this.img2 = img2;
    }

    public String getImg3() {
        return img3;
    }

    public void setImg3(String img3) {
        this.img3 = img3;
    }

    public String getImg4() {
        return img4;
    }

    public void setImg4(String img4) {
        this.img4 = img4;
    }

    public String getImg5() {
        return img5;
    }

    public void setImg5(String img5) {
        this.img5 = img5;
    }
    
    
}

