/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package liniocreateproducts_2017;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author yordyg
 * 
 * private String nombre,marca,modelo,descripcion,color,medidasProducto,pesoProducto,categoriaPrimaria,categoriaAdicional1,precio;
    private String skuVendedor,codigoBarras,cantidad,genero,colorPrincipalProducto,condicionProducto,tiempoGarantia,altoPqt,anchoPqt;
    private String largoPqt,pesoPqt,impuestos,imgPrincipal,img2,img3,img4,img5;
 * 
 */
public class Bolsa extends Product{
    private boolean flatCompleteProduct=true;
    private String error="";

    
    @Override
    boolean validatePhoto(String sku){
        this.setPathPhoto(new File("\\\\192.168.1.34\\FTP_Fotografias\\"+sku+"\\"+sku+"_1.jpg"));
        this.setFlat(this.getPathPhoto().exists());
    return this.getFlat();    
    }
    
    @Override
    boolean validateCompleteProducto(DataBaseConnection connection) {
        if(this.getNombre()==null||this.getNombre().equals("")){
            flatCompleteProduct=false;
            error=error+" falta nombre,";
        }
        if(this.getMarca()==null||this.getMarca().equals("")){
            flatCompleteProduct=false;
            error=error+" falta marca,";
        }
        if(this.getModelo()==null||this.getModelo().equals("")){
            flatCompleteProduct=false;
            error=error+" falta modelo,";
        }
        if(this.getDescripcion()==null||this.getDescripcion().equals("")){
            flatCompleteProduct=false;
            error=error+" falta descripcion,";
        }
        if(this.getColor()==null||this.getColor().equals("")){
            flatCompleteProduct=false;
            error=error+" falta color,";
        }
        if(this.getMedidasProducto()==null||this.getMedidasProducto().equals("")){
//            flatCompleteProduct=false;
//            error=error+" falta medidas del producto,";
            this.setMedidasProducto("1 x 1 x 1");
        }
        if(this.getPesoProducto()==null||this.getPesoProducto().equals("")){
            flatCompleteProduct=false;
            error=error+" falta peso del producto,";
        }
        if(this.getCategoriaPrimaria()==null||this.getCategoriaPrimaria().equals("")){
            flatCompleteProduct=false;
            error=error+" falta categoria primaria,";
        }
        if(this.getCategoriaAdicional1()==null||this.getCategoriaAdicional1().equals("")){
            flatCompleteProduct=false;
            error=error+" falta categoria adicional,";
        }
        if(this.getPrecio()==null||this.getPrecio().equals("")){
            flatCompleteProduct=false;
            error=error+" falta precio,";
        }
        if(this.getSkuVendedor()==null||this.getSkuVendedor().equals("")){
            flatCompleteProduct=false;
            error=error+" falta SKU vendedor,";
        }
        if(this.getCodigoBarras()==null||this.getCodigoBarras().equals("")){
            flatCompleteProduct=false;
            error=error+" falta codigo barras,";
        }
        if(this.getCantidad()==null||this.getCantidad().equals("")){
            flatCompleteProduct=false;
            error=error+" falta cantidad,";
        }
        if(this.getGenero()==null||this.getGenero().equals("")){
            flatCompleteProduct=false;
            error=error+" falta genero,";
        }
        if(this.getColorPrincipalProducto()==null||this.getColorPrincipalProducto().equals("")){
            flatCompleteProduct=false;
            error=error+" falta color principal,";
        }
        if(this.getCondicionProducto()==null||this.getCondicionProducto().equals("")){
            flatCompleteProduct=false;
            error=error+" falta condicion producto,";
        }
        if(this.getTiempoGarantia()==null||this.getTiempoGarantia().equals("")){
            flatCompleteProduct=false;
            error=error+" falta tiempo de garantia,";
        }
        if(this.getAltoPqt()==null||this.getAltoPqt().equals("")){
            flatCompleteProduct=false;
            error=error+" falta alto de pqt,";
        }
        if(this.getAnchoPqt()==null||this.getAnchoPqt().equals("")){
            flatCompleteProduct=false;
            error=error+" falta ancho de pqt,";
        }
        if(this.getLargoPqt()==null||this.getLargoPqt().equals("")){
            flatCompleteProduct=false;
            error=error+" falta largo de pqt,";
        }
        if(this.getPesoPqt()==null||this.getPesoPqt().equals("")){
            flatCompleteProduct=false;
            error=error+" falta peso de pqt,";
        }
        if(this.getImpuestos()==null||this.getImpuestos().equals("")){
            flatCompleteProduct=false;
            error=error+" falta impuesto,";
        }    
        
        //---Send error to data base
        if(flatCompleteProduct==false){
            connection.sendToErrorsTable(connection.getConnection(),this.getSkuVendedor(), error);
        }
    return flatCompleteProduct;
    }
    
    @Override
    void createProduct(Document doc,Element rootElement){
//        System.out.println("-----------");
//        this.setCount(1);    
//        System.out.println(this.getCount()+": -"+this.getNombre());
//        System.out.println("-----------");
        NumberFormat format=new DecimalFormat("#0.00");
        double price;
        
        Element product= doc.createElement("Product");
        rootElement.appendChild(product);
        
        Element sku=doc.createElement("SellerSku");
        sku.appendChild(doc.createCDATASection(this.getSkuVendedor()));
        product.appendChild(sku);
//            Product.addContent(new Element("SellerSku").setText(this.getSkuVendedor()));       /*DePlantillaSkuVendedor*/
//            System.out.println(this.getSkuVendedor());
        Element name=doc.createElement("Name");
        name.appendChild(doc.createCDATASection(this.getNombre()));
        product.appendChild(name);
//            Product.addContent(new Element("Name").setText(this.getNombre()));            /*DePlantillaNombre*/
//            System.out.println(this.getNombre());
        Element quantity=doc.createElement("Quantity");
        quantity.appendChild(doc.createCDATASection(this.getCantidad()));
        product.appendChild(quantity);
//            Product.addContent(new Element("c").setText(this.getCantidad()));                       /*DePlantillaQuantity*/
//            System.out.println(this.getCantidad());
        Element priCategory=doc.createElement("PrimaryCategory");
        priCategory.appendChild(doc.createCDATASection(this.getCategoriaPrimaria()));
        product.appendChild(priCategory);
//            Product.addContent(new Element("PrimaryCategory").setText(this.getCategoriaPrimaria()));            /*DePlantillaPrimary*/
//            System.out.println(this.getCategoriaPrimaria());
        Element adiCategory=doc.createElement("Categories");
        adiCategory.appendChild(doc.createCDATASection(this.getCategoriaAdicional1()));
        product.appendChild(adiCategory);    
//            Product.addContent(new Element("Categories").setText(this.getCategoriaAdicional1()));                      /*DePlantillaAditional*/
//            System.out.println(this.getCategoriaAdicional1());
        Element descrip=doc.createElement("Description");
        descrip.appendChild(doc.createCDATASection(this.getDescripcion()));
        product.appendChild(descrip); 
//            Product.addContent(new Element("Description").setText(this.getDescripcion()));/*DePlantillaDescripcion*/
//            System.out.println(this.getDescripcion());
        Element brand=doc.createElement("Brand");
        brand.appendChild(doc.createCDATASection(this.getMarca()));
        product.appendChild(brand);     
//            Product.addContent(new Element("Brand").setText(this.getMarca()));                       /*DePlantillaMarca*/
//            System.out.println(this.getMarca());
            
        price=Double.parseDouble(this.getPrecio());
        String priceF=format.format(price);
            
        Element pricep=doc.createElement("Price");
        pricep.appendChild(doc.createCDATASection(priceF));
        product.appendChild(pricep);        
//            Product.addContent(new Element("Price").setText(priceF));                       /*DePlantillaPrecio*/
//            System.out.println(priceF);
        Element id=doc.createElement("ProductId");
        id.appendChild(doc.createCDATASection(this.getCodigoBarras()));
        product.appendChild(id);     
//            Product.addContent(new Element("ProductId").setText(this.getCodigoBarras())); //*********************            /*DePlantillaCodigoBarras*/
//            System.out.println(this.getCodigoBarras());
        
        Element tax=doc.createElement("TaxClass");
        tax.appendChild(doc.createCDATASection(this.getImpuestos()));
        product.appendChild(tax); 
//            Product.addContent(new Element("TaxClass").setText(this.getImpuestos()));
//            System.out.println(this.getImpuestos());



            //Product.addContent(new Element("Variation").setText(this.getVariacion()));
            
            //Product.addContent(new Element("Gender").setText(this.getGenero()));
            
            //Product.addContent(new Element("FilterColor").setText(this.getColorPrincipalProducto()));
            
        Element productData=doc.createElement("ProductData");
        product.appendChild(productData);
//                Element ProductData = new Element("ProductData");
        Element pWeight=doc.createElement("PackageWeight");
        pWeight.appendChild(doc.createCDATASection(this.getPesoPqt()));
        productData.appendChild(pWeight);
//                    ProductData.addContent(new Element("PackageWeight").setText(this.getPesoPqt())); 
//                    System.out.println(this.getPesoPqt());
        Element pWidth=doc.createElement("PackageWidth");
        pWidth.appendChild(doc.createCDATASection(this.getAnchoPqt()));
        productData.appendChild(pWidth);
//                    ProductData.addContent(new Element("PackageWidth").setText(this.getAnchoPqt()));          /*DePlantilla2*/
//                    System.out.println(this.getAnchoPqt());
        Element pLength=doc.createElement("PackageLength");
        pLength.appendChild(doc.createCDATASection(this.getLargoPqt()));
        productData.appendChild(pLength);
//                    ProductData.addContent(new Element("PackageLength").setText(this.getLargoPqt()));         /*DePlantilla3*/
//                    System.out.println(this.getLargoPqt());
        Element pHeight=doc.createElement("PackageHeight");
        pHeight.appendChild(doc.createCDATASection(this.getAltoPqt()));
        productData.appendChild(pHeight);
//                    ProductData.addContent(new Element("PackageHeight").setText(this.getAltoPqt()));         /*DePlantilla4*/
//                    System.out.println(this.getAltoPqt());
        Element condition=doc.createElement("ConditionType");
        condition.appendChild(doc.createCDATASection(this.getCondicionProducto()));
        productData.appendChild(condition);
//                    ProductData.addContent(new Element("ConditionType").setText(this.getCondicionProducto()));      /*DePlantillaCondicionProducto*/
//                    System.out.println(this.getCondicionProducto());
        Element color=doc.createElement("Color");
        color.appendChild(doc.createCDATASection(this.getColor()));
        productData.appendChild(color);
//                    ProductData.addContent(new Element("Color").setText(this.getColor()));              /*DePlantilla******/
//                    System.out.println(this.getColor());
                    //ProductData.addContent(new Element("ShortDescription").setText(this.getCodigoBarras()));  //*********************           /*DePlantillaCodigoBarras*/
        Element model=doc.createElement("Model");
        model.appendChild(doc.createCDATASection(this.getModelo()));
        productData.appendChild(model);
//                    ProductData.addContent(new Element("Model").setText(this.getModelo()));              /*DePlantillaModelo*/
//                    System.out.println(this.getModelo());
        Element supplier=doc.createElement("SupplierWarrantyMonths");
        supplier.appendChild(doc.createCDATASection(this.getTiempoGarantia()));
        productData.appendChild(supplier);
//                    ProductData.addContent(new Element("SupplierWarrantyMonths").setText(this.getTiempoGarantia()));
//                    System.out.println(this.getTiempoGarantia());
        Element measures=doc.createElement("ProductMeasures");
        measures.appendChild(doc.createCDATASection(this.getMedidasProducto()));
        productData.appendChild(measures);
//                    ProductData.addContent(new Element("ProductMeasures").setText(this.getMedidasProducto())); 
//                    System.out.println(this.getMedidasProducto());
        Element productWeight=doc.createElement("ProductWeight");
        productWeight.appendChild(doc.createCDATASection(this.getPesoProducto()));
        productData.appendChild(productWeight);
//                    ProductData.addContent(new Element("ProductWeight").setText(this.getPesoProducto())); 
//                    System.out.println(this.getPesoProducto());
        Element gender=doc.createElement("Gender");
        gender.appendChild(doc.createCDATASection(this.getGenero()));
        productData.appendChild(gender);
//                    ProductData.addContent(new Element("Gender").setText(this.getGenero())); 
//                    System.out.println(this.getGenero());
        Element filterColor=doc.createElement("FilterColor");
        filterColor.appendChild(doc.createCDATASection(this.getColorPrincipalProducto()));
        productData.appendChild(filterColor);
//                    ProductData.addContent(new Element("FilterColor").setText(this.getColorPrincipalProducto())); 
//                    System.out.println(this.getColorPrincipalProducto());
                    
//            Product.addContent(ProductData);
            /*FALTA:       -Destacados ; -Tiempo de garantía en meses; #161 */
            //Agregamos todo el la informacion de un articulo al XML
//        doc.getRootElement().addContent(Product);
    }

    
}
