/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sellercenter.dao;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;


/**
 *
 * @author
 */
public class BaseDao {

      private static final String ScApiHost = "https://sellercenter-api.dafiti.com.mx/";          ///"http://bob.dafiti.com.mx/dafiti/webservice/stream/"; PruebasEdgar   ////" https://sellercenter-staging-api.dafiti.com.mx/"; //PRUEBAS            " https://sellercenter-api.dafiti.com.mx/";    //PRODUCTIVO           
      private static final String ScApiHostLinio = "https://sellercenter-api.linio.com.mx/"; //Linio         
      private static final String HASH_ALGORITHM = "HmacSHA256";
      private static final String CHAR_UTF_8 = "UTF-8";
      private static final String CHAR_ASCII = "ASCII";

      public BaseDao() {
      }

      /**
       * calculates the signature and sends the request
       *
       * @param params Map - request parameters
       * @param apiKey String - user's API Key
       * @param XML String - Request Body
       */
      public String getSellercenterApiResponse(Map<String, String> params, String XML) {
            params.put("UserID", "jorgev@oemoda.com");    //  productivo    /// "luzg@oemoda.com");   //PRUEBA           
            params.put("Timestamp", getCurrentTimestamp());
            params.put("Version", "1.0");
            final String apiKey =  "970463fa19efd565b5e48e442de86452befe3205";  //PRODUC  // "319e9341ea9aec28385c47b18c8a571d97d4dba0";    //PRUEBAS                                    "319e9341ea9aec28385c47b18c8a571d97d4dba0";    //PRUEBAS                                                       //"970463fa19efd565b5e48e442de86452befe3205";   productivo           
            String queryString = "";
            String Output = "";
            HttpURLConnection connection = null;
            URL url = null;
            Map<String, String> sortedParams = new TreeMap<String, String>(params);
            queryString = toQueryString(sortedParams);
            String signature = hmacDigest(queryString, apiKey, HASH_ALGORITHM);
            ///  signature = signature.replace("+", " ");
            queryString = queryString.concat("&Signature=".concat(signature));
            //    queryString = queryString.replace("+", "%20");
            String request = ScApiHost.concat("?".concat(queryString));

            try {
                  url = new URL(request);
                  connection = (HttpURLConnection) url.openConnection();
                  connection.setDoOutput(true);
                  connection.setDoInput(true);
                  connection.setInstanceFollowRedirects(false);
                  connection.setRequestMethod("POST");
                  connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                  connection.setRequestProperty("charset", CHAR_UTF_8);
                  connection.setUseCaches(false);
                  if (!XML.equals("")) {
                        connection.setRequestProperty("Content-Length", "" + Integer.toString(XML.getBytes().length));
                        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                        wr.writeBytes(XML);
                        wr.flush();
                        wr.close();
                  }
                  String line;
                  BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                  while ((line = reader.readLine()) != null) {
                        Output += line + "\n";
                  }
            } catch (Exception e) {
                  e.printStackTrace();
            }
            return Output;
      }

   
      
      /**
       * generates hash key
       *
       * @param msg
       * @param keyString
       * @param algo
       * @return string
       */
      private static String hmacDigest(String msg, String keyString, String algo) {
            String digest = null;
            try {
                  SecretKeySpec key = new SecretKeySpec((keyString).getBytes(CHAR_UTF_8), algo);
                  Mac mac = Mac.getInstance(algo);
                  mac.init(key);
                  final byte[] bytes = mac.doFinal(msg.getBytes(CHAR_ASCII));
                  StringBuffer hash = new StringBuffer();
                  for (int i = 0; i < bytes.length; i++) {
                        String hex = Integer.toHexString(0xFF & bytes[i]);
                        if (hex.length() == 1) {
                              hash.append('0');
                        }
                        hash.append(hex);
                  }
                  digest = hash.toString();
            } catch (UnsupportedEncodingException e) {
                  e.printStackTrace();
            } catch (InvalidKeyException e) {
                  e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                  e.printStackTrace();
            }
            return digest;
      }

      /**
       * build querystring out of params map
       *
       * @param data map of params
       * @return string
       * @throws UnsupportedEncodingException
       */
      private static String toQueryString(Map<String, String> data) {
            String queryString = "";
            String xml = "";
            try {
                  StringBuffer params = new StringBuffer();
                  for (Map.Entry<String, String> pair : data.entrySet()) {
                        params.append(URLEncoder.encode((String) pair.getKey(), CHAR_UTF_8) + "=");
                        params.append(URLEncoder.encode((String) pair.getValue(), CHAR_UTF_8) + "&");
                  }
                  if (params.length() > 0) {
                        params.deleteCharAt(params.length() - 1);

                  }
                  queryString = params.toString().replace("+", "%20");
            } catch (UnsupportedEncodingException e) {
                  e.printStackTrace();
            }
            return queryString;
      }


      /**
       * returns the current timestamp
       *
       * @return current timestamp in ISO 8601 format
       */
      private static String getCurrentTimestamp() {
            final TimeZone tz = TimeZone.getTimeZone("UTC");
            final DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
            df.setTimeZone(tz);
            final String nowAsISO = df.format(new Date());
            return nowAsISO;
      }

      public String getSellercenterApiResponseLinio(Map<String, String> params, String XML) {
            params.put("UserID", "yordyg@oemoda.com");                         //"luzg@oemoda.com");    //"api.cloe@cloe.com"
            params.put("Timestamp", getCurrentTimestamp());
            params.put("Version", "1.0");
            final String apiKey = "f5f00489108c1e2b51d593d339773762389affda";                        //"ffdfef9386abd6a6c93afce80208584296f42bcd";       //"ef80f0e0b49085a570bf548673bf9159181e2e20";
            String queryString = "";
            String Output = "";
            HttpURLConnection connection = null;
            URL url = null;
            Map<String, String> sortedParams = new TreeMap<String, String>(params);
            queryString = toQueryString(sortedParams);
            String signature = hmacDigest(queryString, apiKey, HASH_ALGORITHM);
            ///  signature = signature.replace("+", " ");
            queryString = queryString.concat("&Signature=".concat(signature));
            //    queryString = queryString.replace("+", "%20");
            String request = ScApiHostLinio.concat("?".concat(queryString));

            try {
                  url = new URL(request);
                  connection = (HttpURLConnection) url.openConnection();
                  connection.setDoOutput(true);
                  connection.setDoInput(true);
                  connection.setInstanceFollowRedirects(false);
                  connection.setRequestMethod("POST");
                  connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                  connection.setRequestProperty("charset", CHAR_UTF_8);
                  connection.setUseCaches(false);
                  if (!XML.equals("")) {
                        connection.setRequestProperty("Content-Length", "" + Integer.toString(XML.getBytes().length));
//                        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
//                        wr.writeBytes(XML);
//                        wr.flush();
//                        wr.close();
                        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));
                        bw.write(XML);
                        bw.flush();
                        bw.close();
                  }
                  String line;
                  BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                  while ((line = reader.readLine()) != null) {
                        Output += line + "\n";
                  }
            } catch (Exception e) {
                  e.printStackTrace();
            }
            return Output;
      }
}
