/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sellercenter.bss;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sellercenter.entity.Product;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Sistemas
 */
public class ProductBss {

    public ProductBss() {
    }
    
    public List<Product> getProducts (JsonObject body ) {
        
        List<Product> result = new ArrayList<Product>();
        try{        
            
            Gson gson = new Gson();
            JsonObject outerObject = body.getAsJsonObject(); 
            
            for (Map.Entry<String,JsonElement> outerEntry : outerObject.entrySet()) {
                String outerKey = outerEntry.getKey();            
                JsonElement outerValue = outerEntry.getValue();  

                JsonObject innerObject = outerValue.getAsJsonObject(); 
                for (Map.Entry<String,JsonElement> innerEntry : innerObject.entrySet()) {
                    String innerKey = innerEntry.getKey();
                    JsonElement innerValue = innerEntry.getValue();

                    if (innerValue.isJsonArray()){                    
                        JsonArray products = innerValue.getAsJsonArray();
                        for (JsonElement elementProduct : products)
                        {                                            
                            Product product = gson.fromJson(elementProduct, Product.class);
                            result.add(product);
                        }
//                        System.out.println(result.size());
                    }else if(innerValue.isJsonObject()){  
                        JsonObject elementProduct = innerValue.getAsJsonObject(); 
                        Product product = gson.fromJson(elementProduct, Product.class);
                        result.add(product);                       
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return result;
    }
    
    public void postProductCreate () {
    }
    
    public void postProductUpdate () {
    }
    
    public void postProductRemove () {
    }
    
    public void postImage () {
    }
    
    public void getBrands () {
    }
    
    public void getCategoryTree () {
    }
    
    public void getCategoryAttributes () {
    }
    
    public void getCategoriesByAttributeSet () {
    }
    
    
}
