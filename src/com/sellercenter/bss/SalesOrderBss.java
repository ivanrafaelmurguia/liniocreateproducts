/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sellercenter.bss;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sellercenter.entity.AddressBilling;
import com.sellercenter.entity.AddressShipping;
import com.sellercenter.entity.Order;
import com.sellercenter.entity.OrderComments;
import com.sellercenter.entity.OrderItem;
import com.sellercenter.entity.Orders;
import com.sellercenter.entity.StatusToReadyToShipResult;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Sistemas
 */
public class SalesOrderBss {

    public SalesOrderBss() {
    }

    public List<Orders> getOrders(JsonObject body) {

        List<Orders> result = new ArrayList<Orders>();
        try {

            Gson gson = new Gson();
            JsonObject outerObject = body.getAsJsonObject();

            for (Map.Entry<String, JsonElement> outerEntry : outerObject.entrySet()) {
                String outerKey = outerEntry.getKey();
                JsonElement outerValue = outerEntry.getValue();

                JsonObject innerObject = outerValue.getAsJsonObject();
                for (Map.Entry<String, JsonElement> innerEntry : innerObject.entrySet()) {
                    String innerKey = innerEntry.getKey();
                    JsonElement innerValue = innerEntry.getValue();

                    if (innerValue.isJsonArray()) {
                        JsonArray orders = innerValue.getAsJsonArray();
                        for (JsonElement elementOrderO : orders) {
                            JsonObject elementOrder = elementOrderO.getAsJsonObject();
                            Order order = gson.fromJson(elementOrder, Order.class);
                            Orders orderso = new Orders(order);
                            for (Map.Entry<String, JsonElement> deepEntry : elementOrder.entrySet()) {
                                String elementKey = deepEntry.getKey();
                                JsonElement elementValue = deepEntry.getValue();
                                if (elementValue.isJsonPrimitive()) {
                                    System.out.printf("name=%s, value=%s\n", elementKey, elementValue.getAsString());
                                }

                                if (elementValue.isJsonObject()) {
                                    System.out.println("deepKey=" + elementKey);

                                    switch (elementKey) {
                                        case "AddressBilling":
                                            AddressBilling addressBilling = gson.fromJson(elementValue, AddressBilling.class);
                                            orderso.setAddressBilling(addressBilling);
                                            System.out.println("AddressBilling");
                                            break;
                                        case "AddressShipping":
                                            AddressShipping addressShipping = gson.fromJson(elementValue, AddressShipping.class);
                                            orderso.setAddressShipping(addressShipping);
                                            System.out.println("AddressShipping");
                                            break;
                                        case "Statuses":
                                            JsonObject elementStatus = elementValue.getAsJsonObject();
                                            for (Map.Entry<String, JsonElement> statusEntry : elementStatus.entrySet()) {
                                                String key = statusEntry.getKey();
                                                JsonElement value = statusEntry.getValue();
                                                orderso.setStatus(value.getAsString());
                                            }
                                            break;
                                    }

                                }
                            }
                            result.add(orderso);
                        }
//                        System.out.println(result.size());
                    } else if (innerValue.isJsonObject()) {
                        JsonObject elementOrder = innerValue.getAsJsonObject();
                        Order order = gson.fromJson(elementOrder, Order.class);
                        Orders orders = new Orders(order);

                        for (Map.Entry<String, JsonElement> deepEntry : elementOrder.entrySet()) {
                            String elementKey = deepEntry.getKey();
                            JsonElement elementValue = deepEntry.getValue();
                            if (elementValue.isJsonPrimitive()) {
                                System.out.printf("name=%s, value=%s\n", elementKey, elementValue.getAsString());
                            }

                            if (elementValue.isJsonObject()) {
                                System.out.println("deepKey=" + elementKey);

                                switch (elementKey) {
                                    case "AddressBilling":
                                        AddressBilling addressBilling = gson.fromJson(elementValue, AddressBilling.class);
                                        orders.setAddressBilling(addressBilling);
                                        System.out.println("AddressBilling");
                                        break;
                                    case "AddressShipping":
                                        AddressShipping addressShipping = gson.fromJson(elementValue, AddressShipping.class);
                                        orders.setAddressShipping(addressShipping);
                                        System.out.println("AddressShipping");
                                        break;
                                    case "Statuses":
                                        JsonObject elementStatus = elementValue.getAsJsonObject();
                                        for (Map.Entry<String, JsonElement> statusEntry : elementStatus.entrySet()) {
                                            String key = statusEntry.getKey();
                                            JsonElement value = statusEntry.getValue();
                                            orders.setStatus(value.getAsString());
                                        }
                                        break;
                                }

                            }
                        }
                        result.add(orders);
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public void getOrder() {
        //Es la misma funcionalidad que el método getOrders, solo que este
        //regresa la información de una sola orden y no una lista de ordenes
    }

    public List<OrderComments> getOrderComments(JsonObject body) {

        List<OrderComments> result = new ArrayList<OrderComments>();
        try {

            Gson gson = new Gson();
            JsonObject outerObject = body.getAsJsonObject();

            for (Map.Entry<String, JsonElement> outerEntry : outerObject.entrySet()) {
                String outerKey = outerEntry.getKey();
                JsonElement outerValue = outerEntry.getValue();

                JsonObject innerObject = outerValue.getAsJsonObject();
                for (Map.Entry<String, JsonElement> innerEntry : innerObject.entrySet()) {
                    String innerKey = innerEntry.getKey();
                    JsonElement innerValue = innerEntry.getValue();

                    if (innerValue.isJsonArray()) {
                        JsonArray orders = innerValue.getAsJsonArray();
                        for (JsonElement elementOrder : orders) {
                            OrderComments orderItem = gson.fromJson(elementOrder, OrderComments.class);
                            result.add(orderItem);
                        }
//                        System.out.println(result.size());
                    } else if (innerValue.isJsonObject()) {
                        JsonObject elementOrder = innerValue.getAsJsonObject();
                        OrderComments orderItem = gson.fromJson(elementOrder, OrderComments.class);
                        result.add(orderItem);
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public List<OrderItem> getOrderItems(JsonObject body) {

        List<OrderItem> result = new ArrayList<OrderItem>();
        try {

            Gson gson = new Gson();
            JsonObject outerObject = body.getAsJsonObject();

            for (Map.Entry<String, JsonElement> outerEntry : outerObject.entrySet()) {
                String outerKey = outerEntry.getKey();
                JsonElement outerValue = outerEntry.getValue();

                JsonObject innerObject = outerValue.getAsJsonObject();
                for (Map.Entry<String, JsonElement> innerEntry : innerObject.entrySet()) {
                    String innerKey = innerEntry.getKey();
                    JsonElement innerValue = innerEntry.getValue();

                    if (innerValue.isJsonArray()) {
                        JsonArray orders = innerValue.getAsJsonArray();
                        for (JsonElement elementOrder : orders) {
                            OrderItem orderItem = gson.fromJson(elementOrder, OrderItem.class);
                            result.add(orderItem);
                        }
//                        System.out.println(result.size());
                    } else if (innerValue.isJsonObject()) {
                        JsonObject elementOrder = innerValue.getAsJsonObject();
                        OrderItem orderItem = gson.fromJson(elementOrder, OrderItem.class);
                        result.add(orderItem);
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public void getMultipleOrderItems() {
    }

    public void postSetStatusToCanceled() {
    }

    public void postSetStatusToPackedByMarketplace() {
    }

    public List<StatusToReadyToShipResult> postSetStatusToReadyToShip(JsonObject body) {
        
         List<StatusToReadyToShipResult> result = new ArrayList<StatusToReadyToShipResult>();
        try {

            Gson gson = new Gson();
            JsonObject outerObject = body.getAsJsonObject();

            for (Map.Entry<String, JsonElement> outerEntry : outerObject.entrySet()) {
                String outerKey = outerEntry.getKey();
                JsonElement outerValue = outerEntry.getValue();

                JsonObject innerObject = outerValue.getAsJsonObject();
                for (Map.Entry<String, JsonElement> innerEntry : innerObject.entrySet()) {
                    String innerKey = innerEntry.getKey();
                    JsonElement innerValue = innerEntry.getValue();

                    if (innerValue.isJsonArray()) {
                        JsonArray orders = innerValue.getAsJsonArray();
                        for (JsonElement elementOrder : orders) {
                            StatusToReadyToShipResult StatusToReadyToShipResult = gson.fromJson(elementOrder, StatusToReadyToShipResult.class);
                            result.add(StatusToReadyToShipResult);
                        }
//                        System.out.println(result.size());
                    } else if (innerValue.isJsonObject()) {
                        JsonObject elementOrder = innerValue.getAsJsonObject();
                        StatusToReadyToShipResult StatusToReadyToShipResult = gson.fromJson(elementOrder, StatusToReadyToShipResult.class);
                        result.add(StatusToReadyToShipResult);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public void postSetStatusToShipped() {
    }

    public void postSetStatusToFailedDelivery() {
    }

    public void postSetStatusToDelivered() {
    }

    public void getDocument() {
    }

    public void getFailureReasons() {
    }

}
