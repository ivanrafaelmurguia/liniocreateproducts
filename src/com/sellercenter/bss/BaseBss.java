/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sellercenter.bss;

import com.sellercenter.dao.BaseDao;
import java.util.Map;

/**
 *
 * @author 3020178
 */
public class BaseBss {

    public BaseBss() {
    }
    
    /**
    * calculates the signature and sends the request
    *
    * @param params Map - request parameters
    * @param apiKey String - user's API Key
    * @param XML String - Request Body
    */
    public  String getSellercenterApiResponse(Map<String, String> params, String XML) {
    String output = "";
    
    try{
        BaseDao objDao = new BaseDao();
        output = objDao.getSellercenterApiResponse(params, XML);
    }catch(Exception e){
        e.printStackTrace();
    }
    
    return output;
    }
    
    
     public  String getSellercenterApiResponseLinio(Map<String, String> params, String XML) {
    String output = "";
    
    try{
        BaseDao objDao = new BaseDao();
        output = objDao.getSellercenterApiResponseLinio(params, XML);
    }catch(Exception e){
        e.printStackTrace();
    }
    
    return output;
    }
    
  }
