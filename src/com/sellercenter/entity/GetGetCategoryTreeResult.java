/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sellercenter.entity;

/**
 *
 * @author Sistemas
 */
public class GetGetCategoryTreeResult {
    private String Name;
    private String GlobalIdentifier;
    private String CategoryId;//Integer
    private String Children;//Node

    /**
     * @return the Name
     */
    public String getName() {
        return Name;
    }

    /**
     * @param Name the Name to set
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     * @return the GlobalIdentifier
     */
    public String getGlobalIdentifier() {
        return GlobalIdentifier;
    }

    /**
     * @param GlobalIdentifier the GlobalIdentifier to set
     */
    public void setGlobalIdentifier(String GlobalIdentifier) {
        this.GlobalIdentifier = GlobalIdentifier;
    }

    /**
     * @return the CategoryId
     */
    public String getCategoryId() {
        return CategoryId;
    }

    /**
     * @param CategoryId the CategoryId to set
     */
    public void setCategoryId(String CategoryId) {
        this.CategoryId = CategoryId;
    }

    /**
     * @return the Children
     */
    public String getChildren() {
        return Children;
    }

    /**
     * @param Children the Children to set
     */
    public void setChildren(String Children) {
        this.Children = Children;
    }
}
