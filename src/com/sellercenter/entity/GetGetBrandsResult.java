/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sellercenter.entity;

/**
 *
 * @author Sistemas
 */
public class GetGetBrandsResult {
    private String Name;
    private String GlobalIdentifier;

    /**
     * @return the Name
     */
    public String getName() {
        return Name;
    }

    /**
     * @param Name the Name to set
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     * @return the GlobalIdentifier
     */
    public String getGlobalIdentifier() {
        return GlobalIdentifier;
    }

    /**
     * @param GlobalIdentifier the GlobalIdentifier to set
     */
    public void setGlobalIdentifier(String GlobalIdentifier) {
        this.GlobalIdentifier = GlobalIdentifier;
    }
}
