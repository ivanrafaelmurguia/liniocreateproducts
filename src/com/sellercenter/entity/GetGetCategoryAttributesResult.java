/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sellercenter.entity;

/**
 *
 * @author Sistemas
 */
public class GetGetCategoryAttributesResult {
    private String Name;
    private String Label;
    private String IsMandatory;//Boolean
    private String Description;
    private String AttributeType;
    private String ExampleValue;
    private String Options;//Node

    /**
     * @return the Name
     */
    public String getName() {
        return Name;
    }

    /**
     * @param Name the Name to set
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     * @return the Label
     */
    public String getLabel() {
        return Label;
    }

    /**
     * @param Label the Label to set
     */
    public void setLabel(String Label) {
        this.Label = Label;
    }

    /**
     * @return the IsMandatory
     */
    public String getIsMandatory() {
        return IsMandatory;
    }

    /**
     * @param IsMandatory the IsMandatory to set
     */
    public void setIsMandatory(String IsMandatory) {
        this.IsMandatory = IsMandatory;
    }

    /**
     * @return the Description
     */
    public String getDescription() {
        return Description;
    }

    /**
     * @param Description the Description to set
     */
    public void setDescription(String Description) {
        this.Description = Description;
    }

    /**
     * @return the AttributeType
     */
    public String getAttributeType() {
        return AttributeType;
    }

    /**
     * @param AttributeType the AttributeType to set
     */
    public void setAttributeType(String AttributeType) {
        this.AttributeType = AttributeType;
    }

    /**
     * @return the ExampleValue
     */
    public String getExampleValue() {
        return ExampleValue;
    }

    /**
     * @param ExampleValue the ExampleValue to set
     */
    public void setExampleValue(String ExampleValue) {
        this.ExampleValue = ExampleValue;
    }

    /**
     * @return the Options
     */
    public String getOptions() {
        return Options;
    }

    /**
     * @param Options the Options to set
     */
    public void setOptions(String Options) {
        this.Options = Options;
    }
}
