/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sellercenter.entity;

/**
 *
 * @author 
 */
public class Head {
    private String RequestId;
    private String RequestAction;
    private String ResponseType;
    private String Timestamp;
    private String TotalCount;
    private String ErrorType;
    private String ErrorCode;
    private String ErrorMessage;

    /**
     * @return the RequestId
     */
    public String getRequestId() {
        return RequestId;
    }

    /**
     * @param RequestId the RequestId to set
     */
    public void setRequestId(String RequestId) {
        this.RequestId = RequestId;
    }

    /**
     * @return the RequestAction
     */
    public String getRequestAction() {
        return RequestAction;
    }

    /**
     * @param RequestAction the RequestAction to set
     */
    public void setRequestAction(String RequestAction) {
        this.RequestAction = RequestAction;
    }

    /**
     * @return the ResponseType
     */
    public String getResponseType() {
        return ResponseType;
    }

    /**
     * @param ResponseType the ResponseType to set
     */
    public void setResponseType(String ResponseType) {
        this.ResponseType = ResponseType;
    }

    /**
     * @return the Timestamp
     */
    public String getTimestamp() {
        return Timestamp;
    }

    /**
     * @param Timestamp the Timestamp to set
     */
    public void setTimestamp(String Timestamp) {
        this.Timestamp = Timestamp;
    }

    /**
     * @return the TotalCount
     */
    public String getTotalCount() {
        return TotalCount;
    }

    /**
     * @param TotalCount the TotalCount to set
     */
    public void setTotalCount(String TotalCount) {
        this.TotalCount = TotalCount;
    }

    /**
     * @return the ErrorType
     */
    public String getErrorType() {
        return ErrorType;
    }

    /**
     * @param ErrorType the ErrorType to set
     */
    public void setErrorType(String ErrorType) {
        this.ErrorType = ErrorType;
    }

    /**
     * @return the ErrorCode
     */
    public String getErrorCode() {
        return ErrorCode;
    }

    /**
     * @param ErrorCode the ErrorCode to set
     */
    public void setErrorCode(String ErrorCode) {
        this.ErrorCode = ErrorCode;
    }

    /**
     * @return the ErrorMessage
     */
    public String getErrorMessage() {
        return ErrorMessage;
    }

    /**
     * @param ErrorMessage the ErrorMessage to set
     */
    public void setErrorMessage(String ErrorMessage) {
        this.ErrorMessage = ErrorMessage;
    }

}
