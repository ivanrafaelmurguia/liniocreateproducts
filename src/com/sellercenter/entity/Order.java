/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sellercenter.entity;

/**
 *
 * @author Sistemas
 */
public class Order {
    private String TotalCount;//Unsigned
    private String OrderId;//Unsigned
    private String CustomerFirstName;
    private String CustomerLastName;
    private String OrderNumber;//Unsigned
    private String PaymentMethod;  
    private String Remarks;  
    private String DeliveryInfo;  
    private String Price;//Float  
    private String GiftOption;//Boolean  
    private String GiftMessage;  
    private String VoucherCode;  
    private String CreatedAt;//DateTime  
    private String UpdatedAt;//DateTime  
//    private AddressBilling addressBilling;//Subsection  
//    private AddressShipping addressShipping;//Subsection     
    private String NationalRegistrationNumber;  
    private String ItemsCount;//Integer  
//    private String Statuses;//Array  
    private String PromisedShippingTimes;//DateTime  

    /**
     * @return the TotalCount
     */
    public String getTotalCount() {
        return TotalCount;
    }

    /**
     * @param TotalCount the TotalCount to set
     */
    public void setTotalCount(String TotalCount) {
        this.TotalCount = TotalCount;
    }

    /**
     * @return the OrderId
     */
    public String getOrderId() {
        return OrderId;
    }

    /**
     * @param OrderId the OrderId to set
     */
    public void setOrderId(String OrderId) {
        this.OrderId = OrderId;
    }

    /**
     * @return the CustomerFirstName
     */
    public String getCustomerFirstName() {
        return CustomerFirstName;
    }

    /**
     * @param CustomerFirstName the CustomerFirstName to set
     */
    public void setCustomerFirstName(String CustomerFirstName) {
        this.CustomerFirstName = CustomerFirstName;
    }

    /**
     * @return the CustomerLastName
     */
    public String getCustomerLastName() {
        return CustomerLastName;
    }

    /**
     * @param CustomerLastName the CustomerLastName to set
     */
    public void setCustomerLastName(String CustomerLastName) {
        this.CustomerLastName = CustomerLastName;
    }

    /**
     * @return the OrderNumber
     */
    public String getOrderNumber() {
        return OrderNumber;
    }

    /**
     * @param OrderNumber the OrderNumber to set
     */
    public void setOrderNumber(String OrderNumber) {
        this.OrderNumber = OrderNumber;
    }

    /**
     * @return the PaymentMethod
     */
    public String getPaymentMethod() {
        return PaymentMethod;
    }

    /**
     * @param PaymentMethod the PaymentMethod to set
     */
    public void setPaymentMethod(String PaymentMethod) {
        this.PaymentMethod = PaymentMethod;
    }

    /**
     * @return the Remarks
     */
    public String getRemarks() {
        return Remarks;
    }

    /**
     * @param Remarks the Remarks to set
     */
    public void setRemarks(String Remarks) {
        this.Remarks = Remarks;
    }

    /**
     * @return the DeliveryInfo
     */
    public String getDeliveryInfo() {
        return DeliveryInfo;
    }

    /**
     * @param DeliveryInfo the DeliveryInfo to set
     */
    public void setDeliveryInfo(String DeliveryInfo) {
        this.DeliveryInfo = DeliveryInfo;
    }

    /**
     * @return the Price
     */
    public String getPrice() {
        return Price;
    }

    /**
     * @param Price the Price to set
     */
    public void setPrice(String Price) {
        this.Price = Price;
    }

    /**
     * @return the GiftOption
     */
    public String getGiftOption() {
        return GiftOption;
    }

    /**
     * @param GiftOption the GiftOption to set
     */
    public void setGiftOption(String GiftOption) {
        this.GiftOption = GiftOption;
    }

    /**
     * @return the GiftMessage
     */
    public String getGiftMessage() {
        return GiftMessage;
    }

    /**
     * @param GiftMessage the GiftMessage to set
     */
    public void setGiftMessage(String GiftMessage) {
        this.GiftMessage = GiftMessage;
    }

    /**
     * @return the VoucherCode
     */
    public String getVoucherCode() {
        return VoucherCode;
    }

    /**
     * @param VoucherCode the VoucherCode to set
     */
    public void setVoucherCode(String VoucherCode) {
        this.VoucherCode = VoucherCode;
    }

    /**
     * @return the CreatedAt
     */
    public String getCreatedAt() {
        return CreatedAt;
    }

    /**
     * @param CreatedAt the CreatedAt to set
     */
    public void setCreatedAt(String CreatedAt) {
        this.CreatedAt = CreatedAt;
    }

    /**
     * @return the UpdatedAt
     */
    public String getUpdatedAt() {
        return UpdatedAt;
    }

    /**
     * @param UpdatedAt the UpdatedAt to set
     */
    public void setUpdatedAt(String UpdatedAt) {
        this.UpdatedAt = UpdatedAt;
    }

    /**
     * @return the NationalRegistrationNumber
     */
    public String getNationalRegistrationNumber() {
        return NationalRegistrationNumber;
    }

    /**
     * @param NationalRegistrationNumber the NationalRegistrationNumber to set
     */
    public void setNationalRegistrationNumber(String NationalRegistrationNumber) {
        this.NationalRegistrationNumber = NationalRegistrationNumber;
    }

    /**
     * @return the ItemsCount
     */
    public String getItemsCount() {
        return ItemsCount;
    }

    /**
     * @param ItemsCount the ItemsCount to set
     */
    public void setItemsCount(String ItemsCount) {
        this.ItemsCount = ItemsCount;
    }

    /**
     * @return the PromisedShippingTimes
     */
    public String getPromisedShippingTimes() {
        return PromisedShippingTimes;
    }

    /**
     * @param PromisedShippingTimes the PromisedShippingTimes to set
     */
    public void setPromisedShippingTimes(String PromisedShippingTimes) {
        this.PromisedShippingTimes = PromisedShippingTimes;
    }
}
