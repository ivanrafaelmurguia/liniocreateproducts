/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sellercenter.entity;

/**
 *
 * @author Sistemas
 */
public class OrderItem {
    private String OrderItemId;
    private String ShopId;
    private String OrderId;
    private String Name;
    private String Sku;
    private String ShopSku;
    private String ShippingType;
    private String ItemPrice;
    private String PaidPrice;
    private String Currency;
    private String WalletCredits;
    private String TaxAmount;
    private String ShippingAmount;
    private String ShippingServiceCost;
    private String VoucherAmount;
    private String VoucherCode;
    private String Status;
    private String ShipmentProvider;
    private String TrackingCode;
    private String Reason;
    private String ReasonDetail;
    private String PurchaseOrderId;
    private String PurchaseOrderNumber;
    private String PackageId;
    private String PromisedShippingTime;
    private String CreatedAt;
    private String UpdatedAt;

    /**
     * @return the OrderItemId
     */
    public String getOrderItemId() {
        return OrderItemId;
    }

    /**
     * @param OrderItemId the OrderItemId to set
     */
    public void setOrderItemId(String OrderItemId) {
        this.OrderItemId = OrderItemId;
    }

    /**
     * @return the ShopId
     */
    public String getShopId() {
        return ShopId;
    }

    /**
     * @param ShopId the ShopId to set
     */
    public void setShopId(String ShopId) {
        this.ShopId = ShopId;
    }

    /**
     * @return the OrderId
     */
    public String getOrderId() {
        return OrderId;
    }

    /**
     * @param OrderId the OrderId to set
     */
    public void setOrderId(String OrderId) {
        this.OrderId = OrderId;
    }

    /**
     * @return the Name
     */
    public String getName() {
        return Name;
    }

    /**
     * @param Name the Name to set
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     * @return the Sku
     */
    public String getSku() {
        return Sku;
    }

    /**
     * @param Sku the Sku to set
     */
    public void setSku(String Sku) {
        this.Sku = Sku;
    }

    /**
     * @return the ShopSku
     */
    public String getShopSku() {
        return ShopSku;
    }

    /**
     * @param ShopSku the ShopSku to set
     */
    public void setShopSku(String ShopSku) {
        this.ShopSku = ShopSku;
    }

    /**
     * @return the ShippingType
     */
    public String getShippingType() {
        return ShippingType;
    }

    /**
     * @param ShippingType the ShippingType to set
     */
    public void setShippingType(String ShippingType) {
        this.ShippingType = ShippingType;
    }

    /**
     * @return the ItemPrice
     */
    public String getItemPrice() {
        return ItemPrice;
    }

    /**
     * @param ItemPrice the ItemPrice to set
     */
    public void setItemPrice(String ItemPrice) {
        this.ItemPrice = ItemPrice;
    }

    /**
     * @return the PaidPrice
     */
    public String getPaidPrice() {
        return PaidPrice;
    }

    /**
     * @param PaidPrice the PaidPrice to set
     */
    public void setPaidPrice(String PaidPrice) {
        this.PaidPrice = PaidPrice;
    }

    /**
     * @return the Currency
     */
    public String getCurrency() {
        return Currency;
    }

    /**
     * @param Currency the Currency to set
     */
    public void setCurrency(String Currency) {
        this.Currency = Currency;
    }

    /**
     * @return the WalletCredits
     */
    public String getWalletCredits() {
        return WalletCredits;
    }

    /**
     * @param WalletCredits the WalletCredits to set
     */
    public void setWalletCredits(String WalletCredits) {
        this.WalletCredits = WalletCredits;
    }

    /**
     * @return the TaxAmount
     */
    public String getTaxAmount() {
        return TaxAmount;
    }

    /**
     * @param TaxAmount the TaxAmount to set
     */
    public void setTaxAmount(String TaxAmount) {
        this.TaxAmount = TaxAmount;
    }

    /**
     * @return the ShippingAmount
     */
    public String getShippingAmount() {
        return ShippingAmount;
    }

    /**
     * @param ShippingAmount the ShippingAmount to set
     */
    public void setShippingAmount(String ShippingAmount) {
        this.ShippingAmount = ShippingAmount;
    }

    /**
     * @return the ShippingServiceCost
     */
    public String getShippingServiceCost() {
        return ShippingServiceCost;
    }

    /**
     * @param ShippingServiceCost the ShippingServiceCost to set
     */
    public void setShippingServiceCost(String ShippingServiceCost) {
        this.ShippingServiceCost = ShippingServiceCost;
    }

    /**
     * @return the VoucherAmount
     */
    public String getVoucherAmount() {
        return VoucherAmount;
    }

    /**
     * @param VoucherAmount the VoucherAmount to set
     */
    public void setVoucherAmount(String VoucherAmount) {
        this.VoucherAmount = VoucherAmount;
    }

    /**
     * @return the VoucherCode
     */
    public String getVoucherCode() {
        return VoucherCode;
    }

    /**
     * @param VoucherCode the VoucherCode to set
     */
    public void setVoucherCode(String VoucherCode) {
        this.VoucherCode = VoucherCode;
    }

    /**
     * @return the Status
     */
    public String getStatus() {
        return Status;
    }

    /**
     * @param Status the Status to set
     */
    public void setStatus(String Status) {
        this.Status = Status;
    }

    /**
     * @return the ShipmentProvider
     */
    public String getShipmentProvider() {
        return ShipmentProvider;
    }

    /**
     * @param ShipmentProvider the ShipmentProvider to set
     */
    public void setShipmentProvider(String ShipmentProvider) {
        this.ShipmentProvider = ShipmentProvider;
    }

    /**
     * @return the TrackingCode
     */
    public String getTrackingCode() {
        return TrackingCode;
    }

    /**
     * @param TrackingCode the TrackingCode to set
     */
    public void setTrackingCode(String TrackingCode) {
        this.TrackingCode = TrackingCode;
    }

    /**
     * @return the Reason
     */
    public String getReason() {
        return Reason;
    }

    /**
     * @param Reason the Reason to set
     */
    public void setReason(String Reason) {
        this.Reason = Reason;
    }

    /**
     * @return the ReasonDetail
     */
    public String getReasonDetail() {
        return ReasonDetail;
    }

    /**
     * @param ReasonDetail the ReasonDetail to set
     */
    public void setReasonDetail(String ReasonDetail) {
        this.ReasonDetail = ReasonDetail;
    }

    /**
     * @return the PurchaseOrderId
     */
    public String getPurchaseOrderId() {
        return PurchaseOrderId;
    }

    /**
     * @param PurchaseOrderId the PurchaseOrderId to set
     */
    public void setPurchaseOrderId(String PurchaseOrderId) {
        this.PurchaseOrderId = PurchaseOrderId;
    }

    /**
     * @return the PurchaseOrderNumber
     */
    public String getPurchaseOrderNumber() {
        return PurchaseOrderNumber;
    }

    /**
     * @param PurchaseOrderNumber the PurchaseOrderNumber to set
     */
    public void setPurchaseOrderNumber(String PurchaseOrderNumber) {
        this.PurchaseOrderNumber = PurchaseOrderNumber;
    }

    /**
     * @return the PackageId
     */
    public String getPackageId() {
        return PackageId;
    }

    /**
     * @param PackageId the PackageId to set
     */
    public void setPackageId(String PackageId) {
        this.PackageId = PackageId;
    }

    /**
     * @return the PromisedShippingTime
     */
    public String getPromisedShippingTime() {
        return PromisedShippingTime;
    }

    /**
     * @param PromisedShippingTime the PromisedShippingTime to set
     */
    public void setPromisedShippingTime(String PromisedShippingTime) {
        this.PromisedShippingTime = PromisedShippingTime;
    }

    /**
     * @return the CreatedAt
     */
    public String getCreatedAt() {
        return CreatedAt;
    }

    /**
     * @param CreatedAt the CreatedAt to set
     */
    public void setCreatedAt(String CreatedAt) {
        this.CreatedAt = CreatedAt;
    }

    /**
     * @return the UpdatedAt
     */
    public String getUpdatedAt() {
        return UpdatedAt;
    }

    /**
     * @param UpdatedAt the UpdatedAt to set
     */
    public void setUpdatedAt(String UpdatedAt) {
        this.UpdatedAt = UpdatedAt;
    }

}
