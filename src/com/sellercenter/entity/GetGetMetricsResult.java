/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sellercenter.entity;

/**
 *
 * @author Sistemas
 */
public class GetGetMetricsResult {
    private String StatisticsType;
    private String SkuNumber;//Integer
    private String SkuActive;//Integer
    private String SalesTotal;//Integer
    private String Orders;//Integer
    private String Commission;//Integer
    private String ReturnsPercentage;//Decimal
    private String CancellationPercentage;//Decimal

    /**
     * @return the StatisticsType
     */
    public String getStatisticsType() {
        return StatisticsType;
    }

    /**
     * @param StatisticsType the StatisticsType to set
     */
    public void setStatisticsType(String StatisticsType) {
        this.StatisticsType = StatisticsType;
    }

    /**
     * @return the SkuNumber
     */
    public String getSkuNumber() {
        return SkuNumber;
    }

    /**
     * @param SkuNumber the SkuNumber to set
     */
    public void setSkuNumber(String SkuNumber) {
        this.SkuNumber = SkuNumber;
    }

    /**
     * @return the SkuActive
     */
    public String getSkuActive() {
        return SkuActive;
    }

    /**
     * @param SkuActive the SkuActive to set
     */
    public void setSkuActive(String SkuActive) {
        this.SkuActive = SkuActive;
    }

    /**
     * @return the SalesTotal
     */
    public String getSalesTotal() {
        return SalesTotal;
    }

    /**
     * @param SalesTotal the SalesTotal to set
     */
    public void setSalesTotal(String SalesTotal) {
        this.SalesTotal = SalesTotal;
    }

    /**
     * @return the Orders
     */
    public String getOrders() {
        return Orders;
    }

    /**
     * @param Orders the Orders to set
     */
    public void setOrders(String Orders) {
        this.Orders = Orders;
    }

    /**
     * @return the Commission
     */
    public String getCommission() {
        return Commission;
    }

    /**
     * @param Commission the Commission to set
     */
    public void setCommission(String Commission) {
        this.Commission = Commission;
    }

    /**
     * @return the ReturnsPercentage
     */
    public String getReturnsPercentage() {
        return ReturnsPercentage;
    }

    /**
     * @param ReturnsPercentage the ReturnsPercentage to set
     */
    public void setReturnsPercentage(String ReturnsPercentage) {
        this.ReturnsPercentage = ReturnsPercentage;
    }

    /**
     * @return the CancellationPercentage
     */
    public String getCancellationPercentage() {
        return CancellationPercentage;
    }

    /**
     * @param CancellationPercentage the CancellationPercentage to set
     */
    public void setCancellationPercentage(String CancellationPercentage) {
        this.CancellationPercentage = CancellationPercentage;
    }
}
