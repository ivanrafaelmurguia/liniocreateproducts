/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sellercenter.entity;

/**
 *
 * @author Sistemas
 */
public class AddressShipping {
    private String FirstName;
    private String LastName;
    private String Phone;
    private String Phone2;
    private String Address1;
    private String Address2;
    private String Address3;
    private String Address4;
    private String Address5;
    private String City;
    private String Ward;
    private String Region;
    private String PostCode;
    private String Country;

    /**
     * @return the FirstName
     */
    public String getFirstName() {
        return FirstName;
    }

    /**
     * @param FirstName the FirstName to set
     */
    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    /**
     * @return the LastName
     */
    public String getLastName() {
        return LastName;
    }

    /**
     * @param LastName the LastName to set
     */
    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    /**
     * @return the Phone
     */
    public String getPhone() {
        return Phone;
    }

    /**
     * @param Phone the Phone to set
     */
    public void setPhone(String Phone) {
        this.Phone = Phone;
    }

    /**
     * @return the Phone2
     */
    public String getPhone2() {
        return Phone2;
    }

    /**
     * @param Phone2 the Phone2 to set
     */
    public void setPhone2(String Phone2) {
        this.Phone2 = Phone2;
    }

    /**
     * @return the Address1
     */
    public String getAddress1() {
        return Address1;
    }

    /**
     * @param Address1 the Address1 to set
     */
    public void setAddress1(String Address1) {
        this.Address1 = Address1;
    }

    /**
     * @return the Address2
     */
    public String getAddress2() {
        return Address2;
    }

    /**
     * @param Address2 the Address2 to set
     */
    public void setAddress2(String Address2) {
        this.Address2 = Address2;
    }

    /**
     * @return the Address3
     */
    public String getAddress3() {
        return Address3;
    }

    /**
     * @param Address3 the Address3 to set
     */
    public void setAddress3(String Address3) {
        this.Address3 = Address3;
    }

    /**
     * @return the Address4
     */
    public String getAddress4() {
        return Address4;
    }

    /**
     * @param Address4 the Address4 to set
     */
    public void setAddress4(String Address4) {
        this.Address4 = Address4;
    }

    /**
     * @return the Address5
     */
    public String getAddress5() {
        return Address5;
    }

    /**
     * @param Address5 the Address5 to set
     */
    public void setAddress5(String Address5) {
        this.Address5 = Address5;
    }

    /**
     * @return the City
     */
    public String getCity() {
        return City;
    }

    /**
     * @param City the City to set
     */
    public void setCity(String City) {
        this.City = City;
    }

    /**
     * @return the Ward
     */
    public String getWard() {
        return Ward;
    }

    /**
     * @param Ward the Ward to set
     */
    public void setWard(String Ward) {
        this.Ward = Ward;
    }

    /**
     * @return the Region
     */
    public String getRegion() {
        return Region;
    }

    /**
     * @param Region the Region to set
     */
    public void setRegion(String Region) {
        this.Region = Region;
    }

    /**
     * @return the PostCode
     */
    public String getPostCode() {
        return PostCode;
    }

    /**
     * @param PostCode the PostCode to set
     */
    public void setPostCode(String PostCode) {
        this.PostCode = PostCode;
    }

    /**
     * @return the Country
     */
    public String getCountry() {
        return Country;
    }

    /**
     * @param Country the Country to set
     */
    public void setCountry(String Country) {
        this.Country = Country;
    }
    
}
