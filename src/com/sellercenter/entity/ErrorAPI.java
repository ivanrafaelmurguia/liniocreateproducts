/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sellercenter.entity;

/**
 *
 * @author luzg
 */
public class ErrorAPI {
      private String RequestAction;
      private String ErrorType;
      private String ErrorCode;
      private String ErrorMessage;

      /**
       * @return the RequestAction
       */
      public String getRequestAction() {
            return RequestAction;
      }

      /**
       * @param RequestAction the RequestAction to set
       */
      public void setRequestAction(String RequestAction) {
            this.RequestAction = RequestAction;
      }

      /**
       * @return the ErrorType
       */
      public String getErrorType() {
            return ErrorType;
      }

      /**
       * @param ErrorType the ErrorType to set
       */
      public void setErrorType(String ErrorType) {
            this.ErrorType = ErrorType;
      }

      /**
       * @return the ErrorCode
       */
      public String getErrorCode() {
            return ErrorCode;
      }

      /**
       * @param ErrorCode the ErrorCode to set
       */
      public void setErrorCode(String ErrorCode) {
            this.ErrorCode = ErrorCode;
      }

      /**
       * @return the ErrorMessage
       */
      public String getErrorMessage() {
            return ErrorMessage;
      }

      /**
       * @param ErrorMessage the ErrorMessage to set
       */
      public void setErrorMessage(String ErrorMessage) {
            this.ErrorMessage = ErrorMessage;
      }

}
