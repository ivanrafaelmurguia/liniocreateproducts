/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sellercenter.entity;

/**
 *
 * @author Sistemas
 */
public class GetFeedCountResult {
    private String Total;//Integer
    private String Queued;//Integer
    private String Processing;//Integer
    private String Finished;//Integer
    private String Canceled;//Integer

    /**
     * @return the Total
     */
    public String getTotal() {
        return Total;
    }

    /**
     * @param Total the Total to set
     */
    public void setTotal(String Total) {
        this.Total = Total;
    }

    /**
     * @return the Queued
     */
    public String getQueued() {
        return Queued;
    }

    /**
     * @param Queued the Queued to set
     */
    public void setQueued(String Queued) {
        this.Queued = Queued;
    }

    /**
     * @return the Processing
     */
    public String getProcessing() {
        return Processing;
    }

    /**
     * @param Processing the Processing to set
     */
    public void setProcessing(String Processing) {
        this.Processing = Processing;
    }

    /**
     * @return the Finished
     */
    public String getFinished() {
        return Finished;
    }

    /**
     * @param Finished the Finished to set
     */
    public void setFinished(String Finished) {
        this.Finished = Finished;
    }

    /**
     * @return the Canceled
     */
    public String getCanceled() {
        return Canceled;
    }

    /**
     * @param Canceled the Canceled to set
     */
    public void setCanceled(String Canceled) {
        this.Canceled = Canceled;
    }
}
