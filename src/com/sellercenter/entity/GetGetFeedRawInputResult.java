/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sellercenter.entity;

/**
 *
 * @author Sistemas
 */
public class GetGetFeedRawInputResult {
    private String FeedRawInput;//Section
    private String Feed;
    private String RawInputFile;//Section
    private String MimeType;
    private String File;

    /**
     * @return the FeedRawInput
     */
    public String getFeedRawInput() {
        return FeedRawInput;
    }

    /**
     * @param FeedRawInput the FeedRawInput to set
     */
    public void setFeedRawInput(String FeedRawInput) {
        this.FeedRawInput = FeedRawInput;
    }

    /**
     * @return the Feed
     */
    public String getFeed() {
        return Feed;
    }

    /**
     * @param Feed the Feed to set
     */
    public void setFeed(String Feed) {
        this.Feed = Feed;
    }

    /**
     * @return the RawInputFile
     */
    public String getRawInputFile() {
        return RawInputFile;
    }

    /**
     * @param RawInputFile the RawInputFile to set
     */
    public void setRawInputFile(String RawInputFile) {
        this.RawInputFile = RawInputFile;
    }

    /**
     * @return the MimeType
     */
    public String getMimeType() {
        return MimeType;
    }

    /**
     * @param MimeType the MimeType to set
     */
    public void setMimeType(String MimeType) {
        this.MimeType = MimeType;
    }

    /**
     * @return the File
     */
    public String getFile() {
        return File;
    }

    /**
     * @param File the File to set
     */
    public void setFile(String File) {
        this.File = File;
    }
}
