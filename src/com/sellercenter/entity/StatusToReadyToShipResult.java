/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sellercenter.entity;

/**
 *
 * @author Sistemas
 */
public class StatusToReadyToShipResult {
    private String PurchaseOrderId;//Unsigned
    private String PurchaseOrderNumber;

    /**
     * @return the PurchaseOrderId
     */
    public String getPurchaseOrderId() {
        return PurchaseOrderId;
    }

    /**
     * @param PurchaseOrderId the PurchaseOrderId to set
     */
    public void setPurchaseOrderId(String PurchaseOrderId) {
        this.PurchaseOrderId = PurchaseOrderId;
    }

    /**
     * @return the PurchaseOrderNumber
     */
    public String getPurchaseOrderNumber() {
        return PurchaseOrderNumber;
    }

    /**
     * @param PurchaseOrderNumber the PurchaseOrderNumber to set
     */
    public void setPurchaseOrderNumber(String PurchaseOrderNumber) {
        this.PurchaseOrderNumber = PurchaseOrderNumber;
    }
}
