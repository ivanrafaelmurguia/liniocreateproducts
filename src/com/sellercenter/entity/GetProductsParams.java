/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sellercenter.entity;

/**
 *
 * @author Sistemas
 */
public class GetProductsParams {
    private String Action;
    private String Format;
    private String Timestamp; //DateTime
    private String UserID;
    private String Version;
    private String Signature;    

    /**
     * @return the Action
     */
    public String getAction() {
        return Action;
    }

    /**
     * @param Action the Action to set
     */
    public void setAction(String Action) {
        this.Action = Action;
    }

    /**
     * @return the Format
     */
    public String getFormat() {
        return Format;
    }

    /**
     * @param Format the Format to set
     */
    public void setFormat(String Format) {
        this.Format = Format;
    }

    /**
     * @return the Timestamp
     */
    public String getTimestamp() {
        return Timestamp;
    }

    /**
     * @param Timestamp the Timestamp to set
     */
    public void setTimestamp(String Timestamp) {
        this.Timestamp = Timestamp;
    }

    /**
     * @return the UserID
     */
    public String getUserID() {
        return UserID;
    }

    /**
     * @param UserID the UserID to set
     */
    public void setUserID(String UserID) {
        this.UserID = UserID;
    }

    /**
     * @return the Version
     */
    public String getVersion() {
        return Version;
    }

    /**
     * @param Version the Version to set
     */
    public void setVersion(String Version) {
        this.Version = Version;
    }

    /**
     * @return the Signature
     */
    public String getSignature() {
        return Signature;
    }

    /**
     * @param Signature the Signature to set
     */
    public void setSignature(String Signature) {
        this.Signature = Signature;
    }
}
