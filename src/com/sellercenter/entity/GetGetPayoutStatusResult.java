/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sellercenter.entity;

/**
 *
 * @author Sistemas
 */
public class GetGetPayoutStatusResult {
    private String StatementNumber;
    private String CreatedAt;//DateTime
    private String UpdatedAt;//DateTime
    private String OpeningBalance;//Decimal
    private String ItemRevenue;//Decimal
    private String ShipmentFee;//Decimal
    private String ShipmentFeeCredit;//Decimal
    private String OtherRevenueTotal;//Decimal
    private String FeesTotal;//Decimal
    private String Subtotal1;//Decimal
    private String Refunds;//Decimal
    private String FeesOnRefundsTotal;//Decimal
    private String Subtotal2;//Decimal
    private String ClosingBalance;//Decimal
    private String GuaranteeDeposit;//Decimal
    private String Payout;//Decimal
    private String Paid;//Boolean

    /**
     * @return the StatementNumber
     */
    public String getStatementNumber() {
        return StatementNumber;
    }

    /**
     * @param StatementNumber the StatementNumber to set
     */
    public void setStatementNumber(String StatementNumber) {
        this.StatementNumber = StatementNumber;
    }

    /**
     * @return the CreatedAt
     */
    public String getCreatedAt() {
        return CreatedAt;
    }

    /**
     * @param CreatedAt the CreatedAt to set
     */
    public void setCreatedAt(String CreatedAt) {
        this.CreatedAt = CreatedAt;
    }

    /**
     * @return the UpdatedAt
     */
    public String getUpdatedAt() {
        return UpdatedAt;
    }

    /**
     * @param UpdatedAt the UpdatedAt to set
     */
    public void setUpdatedAt(String UpdatedAt) {
        this.UpdatedAt = UpdatedAt;
    }

    /**
     * @return the OpeningBalance
     */
    public String getOpeningBalance() {
        return OpeningBalance;
    }

    /**
     * @param OpeningBalance the OpeningBalance to set
     */
    public void setOpeningBalance(String OpeningBalance) {
        this.OpeningBalance = OpeningBalance;
    }

    /**
     * @return the ItemRevenue
     */
    public String getItemRevenue() {
        return ItemRevenue;
    }

    /**
     * @param ItemRevenue the ItemRevenue to set
     */
    public void setItemRevenue(String ItemRevenue) {
        this.ItemRevenue = ItemRevenue;
    }

    /**
     * @return the ShipmentFee
     */
    public String getShipmentFee() {
        return ShipmentFee;
    }

    /**
     * @param ShipmentFee the ShipmentFee to set
     */
    public void setShipmentFee(String ShipmentFee) {
        this.ShipmentFee = ShipmentFee;
    }

    /**
     * @return the ShipmentFeeCredit
     */
    public String getShipmentFeeCredit() {
        return ShipmentFeeCredit;
    }

    /**
     * @param ShipmentFeeCredit the ShipmentFeeCredit to set
     */
    public void setShipmentFeeCredit(String ShipmentFeeCredit) {
        this.ShipmentFeeCredit = ShipmentFeeCredit;
    }

    /**
     * @return the OtherRevenueTotal
     */
    public String getOtherRevenueTotal() {
        return OtherRevenueTotal;
    }

    /**
     * @param OtherRevenueTotal the OtherRevenueTotal to set
     */
    public void setOtherRevenueTotal(String OtherRevenueTotal) {
        this.OtherRevenueTotal = OtherRevenueTotal;
    }

    /**
     * @return the FeesTotal
     */
    public String getFeesTotal() {
        return FeesTotal;
    }

    /**
     * @param FeesTotal the FeesTotal to set
     */
    public void setFeesTotal(String FeesTotal) {
        this.FeesTotal = FeesTotal;
    }

    /**
     * @return the Subtotal1
     */
    public String getSubtotal1() {
        return Subtotal1;
    }

    /**
     * @param Subtotal1 the Subtotal1 to set
     */
    public void setSubtotal1(String Subtotal1) {
        this.Subtotal1 = Subtotal1;
    }

    /**
     * @return the Refunds
     */
    public String getRefunds() {
        return Refunds;
    }

    /**
     * @param Refunds the Refunds to set
     */
    public void setRefunds(String Refunds) {
        this.Refunds = Refunds;
    }

    /**
     * @return the FeesOnRefundsTotal
     */
    public String getFeesOnRefundsTotal() {
        return FeesOnRefundsTotal;
    }

    /**
     * @param FeesOnRefundsTotal the FeesOnRefundsTotal to set
     */
    public void setFeesOnRefundsTotal(String FeesOnRefundsTotal) {
        this.FeesOnRefundsTotal = FeesOnRefundsTotal;
    }

    /**
     * @return the Subtotal2
     */
    public String getSubtotal2() {
        return Subtotal2;
    }

    /**
     * @param Subtotal2 the Subtotal2 to set
     */
    public void setSubtotal2(String Subtotal2) {
        this.Subtotal2 = Subtotal2;
    }

    /**
     * @return the ClosingBalance
     */
    public String getClosingBalance() {
        return ClosingBalance;
    }

    /**
     * @param ClosingBalance the ClosingBalance to set
     */
    public void setClosingBalance(String ClosingBalance) {
        this.ClosingBalance = ClosingBalance;
    }

    /**
     * @return the GuaranteeDeposit
     */
    public String getGuaranteeDeposit() {
        return GuaranteeDeposit;
    }

    /**
     * @param GuaranteeDeposit the GuaranteeDeposit to set
     */
    public void setGuaranteeDeposit(String GuaranteeDeposit) {
        this.GuaranteeDeposit = GuaranteeDeposit;
    }

    /**
     * @return the Payout
     */
    public String getPayout() {
        return Payout;
    }

    /**
     * @param Payout the Payout to set
     */
    public void setPayout(String Payout) {
        this.Payout = Payout;
    }

    /**
     * @return the Paid
     */
    public String getPaid() {
        return Paid;
    }

    /**
     * @param Paid the Paid to set
     */
    public void setPaid(String Paid) {
        this.Paid = Paid;
    }
}
