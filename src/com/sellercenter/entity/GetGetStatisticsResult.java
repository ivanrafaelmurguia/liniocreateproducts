/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sellercenter.entity;

/**
 *
 * @author Sistemas
 */
public class GetGetStatisticsResult {
//PRODUCTS
    private String Active;//Integer
    private String All;//Integer
    private String Deleted;//Integer
    private String ImageMissing;//Integer
    private String Inactive;//Integer
    private String Live;//Integer
    private String Pending;//Integer
    private String PoorQuality;//Integer
    private String SoldOut;//Integer
    private String Total;//Integer
    
//ORDERS
    private String Canceled;//Integer
    private String Delivered;//Integer
    private String Economy;//Integer
    private String Express;//Integer
    private String Failed;//Integer
    private String NoExtInvoiceKey;//Integer
    private String NotPrintedPending;//Integer
    private String NotPrintedReadyToShip;//Integer
//    private String pending;//Integer
    private String Processing;//Integer
    private String ReadyToShip;//Integer
    private String ReturnRejected;//Integer
    private String ReturnShippedByCustomer;//Integer
    private String ReturnWaitingForApproval;//Integer
    private String Returned;//Integer
    private String Shipped;//Integer
    private String Standard;//Integer
//    private String total;//Integer
    
//ORDER ITEMS PENDING
    private String Today;//Integer
    private String Yesterday;//Integer
    private String Older;//Integer
    
 //ACCOUNT HEALTH   
    private String Percentage;//Decimal
    private String Text;
}
