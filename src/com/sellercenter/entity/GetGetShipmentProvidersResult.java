/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sellercenter.entity;

/**
 *
 * @author Sistemas
 */
public class GetGetShipmentProvidersResult {
    private String Name;
    private String DefaultValue;//Boolean
    private String ApiIntegration;//Boolean
    private String Cod;//Boolean
    private String TrackingCodeValidationRegex;
    private String TrackingCodeExample;
    private String TrackingUrl;

    /**
     * @return the Name
     */
    public String getName() {
        return Name;
    }

    /**
     * @param Name the Name to set
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     * @return the DefaultValue
     */
    public String getDefaultValue() {
        return DefaultValue;
    }

    /**
     * @param DefaultValue the DefaultValue to set
     */
    public void setDefaultValue(String DefaultValue) {
        this.DefaultValue = DefaultValue;
    }

    /**
     * @return the ApiIntegration
     */
    public String getApiIntegration() {
        return ApiIntegration;
    }

    /**
     * @param ApiIntegration the ApiIntegration to set
     */
    public void setApiIntegration(String ApiIntegration) {
        this.ApiIntegration = ApiIntegration;
    }

    /**
     * @return the Cod
     */
    public String getCod() {
        return Cod;
    }

    /**
     * @param Cod the Cod to set
     */
    public void setCod(String Cod) {
        this.Cod = Cod;
    }

    /**
     * @return the TrackingCodeValidationRegex
     */
    public String getTrackingCodeValidationRegex() {
        return TrackingCodeValidationRegex;
    }

    /**
     * @param TrackingCodeValidationRegex the TrackingCodeValidationRegex to set
     */
    public void setTrackingCodeValidationRegex(String TrackingCodeValidationRegex) {
        this.TrackingCodeValidationRegex = TrackingCodeValidationRegex;
    }

    /**
     * @return the TrackingCodeExample
     */
    public String getTrackingCodeExample() {
        return TrackingCodeExample;
    }

    /**
     * @param TrackingCodeExample the TrackingCodeExample to set
     */
    public void setTrackingCodeExample(String TrackingCodeExample) {
        this.TrackingCodeExample = TrackingCodeExample;
    }

    /**
     * @return the TrackingUrl
     */
    public String getTrackingUrl() {
        return TrackingUrl;
    }

    /**
     * @param TrackingUrl the TrackingUrl to set
     */
    public void setTrackingUrl(String TrackingUrl) {
        this.TrackingUrl = TrackingUrl;
    }
}
