/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sellercenter.entity;

/**
 *
 * @author Sistemas
 */
public class OrderComments {
    private String CommentId;//Unsigned
    private String Username;
    private String Content;
    private String Type;//Enum
    private String IsOpened;  
    private String IsAnswered;    
    private String CreatedAt;//DateTime  
    private String UpdatedAt;//DateTime   
    private String Comments;//Array   

    /**
     * @return the CommentId
     */
    public String getCommentId() {
        return CommentId;
    }

    /**
     * @param CommentId the CommentId to set
     */
    public void setCommentId(String CommentId) {
        this.CommentId = CommentId;
    }

    /**
     * @return the Username
     */
    public String getUsername() {
        return Username;
    }

    /**
     * @param Username the Username to set
     */
    public void setUsername(String Username) {
        this.Username = Username;
    }

    /**
     * @return the Content
     */
    public String getContent() {
        return Content;
    }

    /**
     * @param Content the Content to set
     */
    public void setContent(String Content) {
        this.Content = Content;
    }

    /**
     * @return the Type
     */
    public String getType() {
        return Type;
    }

    /**
     * @param Type the Type to set
     */
    public void setType(String Type) {
        this.Type = Type;
    }

    /**
     * @return the IsOpened
     */
    public String getIsOpened() {
        return IsOpened;
    }

    /**
     * @param IsOpened the IsOpened to set
     */
    public void setIsOpened(String IsOpened) {
        this.IsOpened = IsOpened;
    }

    /**
     * @return the IsAnswered
     */
    public String getIsAnswered() {
        return IsAnswered;
    }

    /**
     * @param IsAnswered the IsAnswered to set
     */
    public void setIsAnswered(String IsAnswered) {
        this.IsAnswered = IsAnswered;
    }

    /**
     * @return the createdAt
     */
    public String getCreatedAt() {
        return CreatedAt;
    }

    /**
     * @param CreatedAt the CreatedAt to set
     */
    public void setCreatedAt(String CreatedAt) {
        this.CreatedAt = CreatedAt;
    }

    /**
     * @return the updatedAt
     */
    public String getUpdatedAt() {
        return UpdatedAt;
    }

    /**
     * @param UpdatedAt the UpdatedAt to set
     */
    public void setUpdatedAt(String UpdatedAt) {
        this.UpdatedAt = UpdatedAt;
    }

    /**
     * @return the Comments
     */
    public String getComments() {
        return Comments;
    }

    /**
     * @param Comments the Comments to set
     */
    public void setComments(String Comments) {
        this.Comments = Comments;
    }
}
