/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sellercenter.entity;

/**
 *
 * @author Sistemas
 */
public class GetFeedOffsetListResult {
    private String Feed;//UUID
    private String Status;
    private String Action;
    private String CreationDate;//TimeDate
    private String UpdatedDate;//TimeDate
    private String Source;
    private String TotalRecords;//Integer
    private String ProcessedRecords;//Integer
    private String FailedRecords;//Embedded CSV File

    /**
     * @return the Feed
     */
    public String getFeed() {
        return Feed;
    }

    /**
     * @param Feed the Feed to set
     */
    public void setFeed(String Feed) {
        this.Feed = Feed;
    }

    /**
     * @return the Status
     */
    public String getStatus() {
        return Status;
    }

    /**
     * @param Status the Status to set
     */
    public void setStatus(String Status) {
        this.Status = Status;
    }

    /**
     * @return the Action
     */
    public String getAction() {
        return Action;
    }

    /**
     * @param Action the Action to set
     */
    public void setAction(String Action) {
        this.Action = Action;
    }

    /**
     * @return the CreationDate
     */
    public String getCreationDate() {
        return CreationDate;
    }

    /**
     * @param CreationDate the CreationDate to set
     */
    public void setCreationDate(String CreationDate) {
        this.CreationDate = CreationDate;
    }

    /**
     * @return the UpdatedDate
     */
    public String getUpdatedDate() {
        return UpdatedDate;
    }

    /**
     * @param UpdatedDate the UpdatedDate to set
     */
    public void setUpdatedDate(String UpdatedDate) {
        this.UpdatedDate = UpdatedDate;
    }

    /**
     * @return the Source
     */
    public String getSource() {
        return Source;
    }

    /**
     * @param Source the Source to set
     */
    public void setSource(String Source) {
        this.Source = Source;
    }

    /**
     * @return the TotalRecords
     */
    public String getTotalRecords() {
        return TotalRecords;
    }

    /**
     * @param TotalRecords the TotalRecords to set
     */
    public void setTotalRecords(String TotalRecords) {
        this.TotalRecords = TotalRecords;
    }

    /**
     * @return the ProcessedRecords
     */
    public String getProcessedRecords() {
        return ProcessedRecords;
    }

    /**
     * @param ProcessedRecords the ProcessedRecords to set
     */
    public void setProcessedRecords(String ProcessedRecords) {
        this.ProcessedRecords = ProcessedRecords;
    }

    /**
     * @return the FailedRecords
     */
    public String getFailedRecords() {
        return FailedRecords;
    }

    /**
     * @param FailedRecords the FailedRecords to set
     */
    public void setFailedRecords(String FailedRecords) {
        this.FailedRecords = FailedRecords;
    }
}
