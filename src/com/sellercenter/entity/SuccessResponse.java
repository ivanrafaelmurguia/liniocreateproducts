/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sellercenter.entity;

/**
 *
 * @author 3020178
 */
public class SuccessResponse {
    private String name;
    private Head[] head;
    private Body[] body;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the head
     */
    public Head[] getHead() {
        return head;
    }

    /**
     * @param head the head to set
     */
    public void setHead(Head[] head) {
        this.head = head;
    }

    /**
     * @return the body
     */
    public Body[] getBody() {
        return body;
    }

    /**
     * @param body the body to set
     */
    public void setBody(Body[] body) {
        this.body = body;
    }
}
