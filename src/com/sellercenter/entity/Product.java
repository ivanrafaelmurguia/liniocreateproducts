/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sellercenter.entity;

/**
 *
 * @author Sistemas
 */
public class Product {
    private String SellerSku;
    private String ParentSku;
    private String Status;
    private String Name;
    private String Variation;
    private String PrimaryCategory;//Integer
    private String Categories;
    private String BrowseNodes;
    private String Description;
    private String Brand;
    private String Price;//Decimal
    private String SalePrice;//Decimal
    private String SaleStartDate;//DateTime
    private String SaleEndDate;//DateTime
    private String TaxClass;
    private String ShipmentType;
    private String ProductId;
    private String Condition;
    private String ProductData;//Subsection
    private String Quantity;//Integer

    /**
     * @return the SellerSku
     */
    public String getSellerSku() {
        return SellerSku;
    }

    /**
     * @param SellerSku the SellerSku to set
     */
    public void setSellerSku(String SellerSku) {
        this.SellerSku = SellerSku;
    }

    /**
     * @return the ParentSku
     */
    public String getParentSku() {
        return ParentSku;
    }

    /**
     * @param ParentSku the ParentSku to set
     */
    public void setParentSku(String ParentSku) {
        this.ParentSku = ParentSku;
    }

    /**
     * @return the Status
     */
    public String getStatus() {
        return Status;
    }

    /**
     * @param Status the Status to set
     */
    public void setStatus(String Status) {
        this.Status = Status;
    }

    /**
     * @return the Name
     */
    public String getName() {
        return Name;
    }

    /**
     * @param Name the Name to set
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     * @return the Variation
     */
    public String getVariation() {
        return Variation;
    }

    /**
     * @param Variation the Variation to set
     */
    public void setVariation(String Variation) {
        this.Variation = Variation;
    }

    /**
     * @return the PrimaryCategory
     */
    public String getPrimaryCategory() {
        return PrimaryCategory;
    }

    /**
     * @param PrimaryCategory the PrimaryCategory to set
     */
    public void setPrimaryCategory(String PrimaryCategory) {
        this.PrimaryCategory = PrimaryCategory;
    }

    /**
     * @return the Categories
     */
    public String getCategories() {
        return Categories;
    }

    /**
     * @param Categories the Categories to set
     */
    public void setCategories(String Categories) {
        this.Categories = Categories;
    }

    /**
     * @return the BrowseNodes
     */
    public String getBrowseNodes() {
        return BrowseNodes;
    }

    /**
     * @param BrowseNodes the BrowseNodes to set
     */
    public void setBrowseNodes(String BrowseNodes) {
        this.BrowseNodes = BrowseNodes;
    }

    /**
     * @return the Description
     */
    public String getDescription() {
        return Description;
    }

    /**
     * @param Description the Description to set
     */
    public void setDescription(String Description) {
        this.Description = Description;
    }

    /**
     * @return the Brand
     */
    public String getBrand() {
        return Brand;
    }

    /**
     * @param Brand the Brand to set
     */
    public void setBrand(String Brand) {
        this.Brand = Brand;
    }

    /**
     * @return the Price
     */
    public String getPrice() {
        return Price;
    }

    /**
     * @param Price the Price to set
     */
    public void setPrice(String Price) {
        this.Price = Price;
    }

    /**
     * @return the SalePrice
     */
    public String getSalePrice() {
        return SalePrice;
    }

    /**
     * @param SalePrice the SalePrice to set
     */
    public void setSalePrice(String SalePrice) {
        this.SalePrice = SalePrice;
    }

    /**
     * @return the SaleStartDate
     */
    public String getSaleStartDate() {
        return SaleStartDate;
    }

    /**
     * @param SaleStartDate the SaleStartDate to set
     */
    public void setSaleStartDate(String SaleStartDate) {
        this.SaleStartDate = SaleStartDate;
    }

    /**
     * @return the SaleEndDate
     */
    public String getSaleEndDate() {
        return SaleEndDate;
    }

    /**
     * @param SaleEndDate the SaleEndDate to set
     */
    public void setSaleEndDate(String SaleEndDate) {
        this.SaleEndDate = SaleEndDate;
    }

    /**
     * @return the TaxClass
     */
    public String getTaxClass() {
        return TaxClass;
    }

    /**
     * @param TaxClass the TaxClass to set
     */
    public void setTaxClass(String TaxClass) {
        this.TaxClass = TaxClass;
    }

    /**
     * @return the ShipmentType
     */
    public String getShipmentType() {
        return ShipmentType;
    }

    /**
     * @param ShipmentType the ShipmentType to set
     */
    public void setShipmentType(String ShipmentType) {
        this.ShipmentType = ShipmentType;
    }

    /**
     * @return the ProductId
     */
    public String getProductId() {
        return ProductId;
    }

    /**
     * @param ProductId the ProductId to set
     */
    public void setProductId(String ProductId) {
        this.ProductId = ProductId;
    }

    /**
     * @return the Condition
     */
    public String getCondition() {
        return Condition;
    }

    /**
     * @param Condition the Condition to set
     */
    public void setCondition(String Condition) {
        this.Condition = Condition;
    }

    /**
     * @return the ProductData
     */
    public String getProductData() {
        return ProductData;
    }

    /**
     * @param ProductData the ProductData to set
     */
    public void setProductData(String ProductData) {
        this.ProductData = ProductData;
    }

    /**
     * @return the Quantity
     */
    public String getQuantity() {
        return Quantity;
    }

    /**
     * @param Quantity the Quantity to set
     */
    public void setQuantity(String Quantity) {
        this.Quantity = Quantity;
    }

}
